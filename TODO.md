# To Dos

* DONE Remap symbols (e.g. map ANS holdings to NEO and vice versa, or just replace all NEO with ANS)
* The CoinTracking converter randomly orders trades in the same minute. We need to prioritize buys before sells.