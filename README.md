# Crypto Tax Validator
Validates trades, openings, spending, and income statements in Bitcoin.tax format, chiefly by looking for deficits in holdings at any point in time.

## Instructions
To run, execute `python main.py` and browse to http://127.0.0.1:14200.
