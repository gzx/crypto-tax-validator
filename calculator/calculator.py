# coding: utf-8
# pylint: disable=missing-class-docstring, missing-function-docstring, line-too-long, superfluous-parens, trailing-whitespace, 
# pylint: disable=invalid-name, no-self-use, logging-fstring-interpolation, too-many-locals, missing-module-docstring, too-many-arguments
# pylint: disable=too-many-branches, missing-final-newline
import random
import time
import datetime as dt
import uuid
import os
import logging
from dateutil import parser as dtparser
import pandas as pd
import numpy as np
import dateparser as dtp
import matplotlib as mpl
import matplotlib.pyplot as plt

from config import CURRENCIES, STABLE_COINS, QUOTE_COINS, STABLE_COINS, SAFE_CURRENCIES, SAFE_PRICE_CURRENCIES, CURR_EXCHANGE_DICT, WEIRD_BASE_COINS, SAFE_EXCHANGES, SAFE_TRADE_DATES

plt.style.use('ggplot')
mpl.rcParams['figure.figsize'] = (18.0, 10.0)

pd.set_option('precision',12)
pd.set_option('display.float_format', lambda x: '%.12f' % x)
pd.set_option('display.max_rows', 250)
pd.set_option('expand_frame_repr', False)

log = logging.getLogger()


class Calculator(object):

    def __init__(self, base_dir, client_code, tax_year, 
                 price_provider, 
                 calc_method, fiat_calc_method,
                 debug_date=None, debug_coin=None):
        self.base_dir = base_dir
        self.client_code = client_code
        self.tax_year = tax_year
        self.p = price_provider
        assert(calc_method in ['fifo', 'lifo', 'acb'])
        assert(fiat_calc_method in ['fifo', 'lifo', 'acb'])
        self.calc_method = calc_method
        self.fiat_calc_method = fiat_calc_method
        self.debug_date = debug_date 
        self.debug_coin = debug_coin


    def get_year_from_row_date(self, rowdate):
        return dtp.parse(rowdate[:-5]).year

    def add_to_8949(self, form8949, row, coin, pool_orig_acq_date, trade_cost_basis, amt_bought_in_txn, 
                    bought_coin, amt_used_in_txn, used_coin, total_amt_received_in_trade):
        """
        Adds "crypto to crypto" txn to 8949 form.
        row: row from pandas.DataFrame of trades/income/spending
        coin: str of coin symbol
        pool_orig_acq_date: str of date coin was originally acquired
        trade_cost_basis: float of the cost of the trade
        """
        given_up_amt_in_txn = amt_used_in_txn
        received_amt_in_txn = amt_bought_in_txn
        fmv, coin_p, base_usd_p, used_base = self.get_received_usd_value_in_trade(bought_coin, row, received_amt_in_txn,
                                                                            total_amt_received_in_trade)    
        # if the trade is for basically nothing, don't add it
        if round(given_up_amt_in_txn, 8) == 0.0 and round(given_up_amt_in_txn, 8) == 0.0:
            return
        line_item_8949 = {
            "Description (a)" : str(round(given_up_amt_in_txn, 8)) + " " + used_coin,
            "Date Acquired (b)" : pool_orig_acq_date,
            "Date Sold (c)" : row['Date'],
            "Proceeds (d)" : round(fmv, 8),
            "Cost Basis (e)" : round(trade_cost_basis, 8),
            "Adjustment Code (f)" : "",
            "Adjustment amount (g)" : "",
            "Gain or loss (h)" : round(fmv - trade_cost_basis, 8),
        }
        form8949 += [line_item_8949]


    def add_new_pool_acb(self, pool, fifo_queue_dict, row, old_pool_key, sold_from_pool_total_cost_basis, old_pool_acq_date,
                         old_pool_amt, used_coin, used_amt_in_txn, bought_coin, bought_amt_in_txn, is_pool_depleted):
        """
        Adds coin bought in trade `row` to fifo queue `fifo_queue_dict`. Preserves original acquiry date from the
        `sold_from_pool` pool, which contains the coin that was sold to buy `bought_amt` units of `bought_coin`.
        Returns cost_basis to be subtracted from cost_basis of old_pool. Puts subtracted cost_basis in new pool.
        IMPORTANT: if same date as existing pool, simply adds back to original pool
        """
        trade_year = self.get_year_from_row_date(row['Date'])
        # add bought to fifo queue
        log.debug('adding %f %s',bought_amt_in_txn, bought_coin)
        log.debug("We're selling %f %s out of %f %s in pool", used_amt_in_txn, used_coin, old_pool_amt, used_coin)
        cost_basis = self.get_avg_cost_basis(used_coin, pool, fifo_queue_dict) * (used_amt_in_txn / pool['amt'])
        log.debug("Cost basis of used %f %s sold is %f", used_amt_in_txn, used_coin, cost_basis)
        log.debug("Origin date/pool: %s %s", old_pool_key, old_pool_acq_date)
        if trade_year < 2018:
            new_pool = {
                "acq_date": old_pool_acq_date, 
                "trade_date": row['Date'],
                "key": old_pool_key,
                "amt": bought_amt_in_txn, 
                "usd_price": cost_basis/bought_amt_in_txn, 
                "cost_basis": cost_basis
            }
        else:
            total_amt_received_in_trade = row['Volume'] if row['Action'] == "BUY" else row['Cost/Proceeds']
            fmv, coin_p, base_usd_p, used_base = self.get_received_usd_value_in_trade(bought_coin, row, bought_amt_in_txn,
                                                                                total_amt_received_in_trade)
            new_pool = {
                "acq_date": row['Date'],
                "trade_date": row['Date'],
                "key": old_pool_key,
                "amt": bought_amt_in_txn, 
                "usd_price": cost_basis/bought_amt_in_txn, 
                "cost_basis": fmv,
                "Coin price in currency": coin_p,
                "Currency price": base_usd_p,
                "Error - used base": used_base
            }
        if bought_coin in fifo_queue_dict:
            fifo_queue_dict[bought_coin].insert(0, new_pool)
        else:
            fifo_queue_dict[bought_coin] = [new_pool]
        # print info about pools
        coin_pools = [d["amt"] for d in fifo_queue_dict[bought_coin]]
        total_pool_amt = sum(coin_pools)
        num_pools = len(coin_pools)
        log.debug('after buy, now holding %f %s in %d pools', total_pool_amt, bought_coin, num_pools)
        return cost_basis

        
    def add_new_pool(self, fifo_queue_dict, row, old_pool_key, sold_from_pool_total_cost_basis, old_pool_acq_date,
                     old_pool_amt, used_coin, used_amt_in_txn, bought_coin, bought_amt_in_txn, is_pool_depleted):
        """
        Adds coin bought in trade `row` to fifo queue `fifo_queue_dict`. Preserves original acquiry date from the
        `sold_from_pool` pool, which contains the coin that was sold to buy `bought_amt` units of `bought_coin`.
        Returns cost_basis to be subtracted from cost_basis of old_pool. Puts subtracted cost_basis in new pool.
        IMPORTANT: if same date as existing pool, simply adds back to original pool
        """
        trade_year = self.get_year_from_row_date(row['Date'])
        # add bought to fifo queue
        log.debug('adding %f %s', bought_amt_in_txn, bought_coin)
        log.debug("We're selling %f %s out of %f %s in pool", used_amt_in_txn, used_coin, old_pool_amt, used_coin)
        if is_pool_depleted:
            cost_basis = sold_from_pool_total_cost_basis
            if used_amt_in_txn != old_pool_amt:
                raise Exception('ERROR: pool depleted but used amt does not match pool amt!')
        else:
            cost_basis = sold_from_pool_total_cost_basis * (used_amt_in_txn / old_pool_amt)
        log.debug("Cost basis of used %f %s sold is %f", used_amt_in_txn, used_coin, cost_basis)
        log.debug("Origin date/pool: %s %s", old_pool_key, old_pool_acq_date)
        if trade_year < 2018:
            new_pool = {
                "acq_date": old_pool_acq_date, 
                "trade_date": row['Date'],
                "key": old_pool_key,
                "amt": bought_amt_in_txn, 
                "usd_price": cost_basis/bought_amt_in_txn, 
                "cost_basis": cost_basis
            }
        else:
            total_amt_received_in_trade = row['Volume'] if row['Action'] == "BUY" else row['Cost/Proceeds']
            fmv, coin_p, base_usd_p, used_base = self.get_received_usd_value_in_trade(bought_coin, row, bought_amt_in_txn,
                                                                                total_amt_received_in_trade)
            new_pool = {
                "acq_date": row['Date'],
                "trade_date": row['Date'],
                "key": old_pool_key,
                "amt": bought_amt_in_txn, 
                "usd_price": cost_basis/bought_amt_in_txn, 
                "cost_basis": fmv,
                "Coin price in currency": coin_p,
                "Currency price": base_usd_p,
                "Error - used base": used_base
            }
        if bought_coin in fifo_queue_dict:
            fifo_queue_dict[bought_coin].insert(0, new_pool)
        else:
            fifo_queue_dict[bought_coin] = [new_pool]
        # print info about pools
        coin_pools = [d["amt"] for d in fifo_queue_dict[bought_coin]]
        total_pool_amt = sum(coin_pools)
        num_pools = len(coin_pools)
        log.debug('after buy, now holding %f %s in %d pools', total_pool_amt, bought_coin, num_pools)
        return cost_basis

    def get_avg_cost(self, coin, queue_dict):
        """
        Gets the avg cost basis in the coin pools contained in `queue_dict`, weighting each pool cost basis
        by the amt of coins held in each pool
        """
        pools = queue_dict[coin]
        sum_weighted_costs = 0
        sum_amts = 0
        for p in pools:
            sum_weighted_costs += (p['cost_basis']/p['amt']) * p['amt']
            sum_amts += p['amt']
        avg_cost = sum_weighted_costs / sum_amts
        log.debug('avg cost basis computed for %s is %f', coin, avg_cost)
        return avg_cost


    def get_avg_cost_basis(self, coin, tradepool, queue_dict):
        """
        Gets the average cost basis of `tradepool` by computing the avg cost basis in the coin pools contained in
        `queue_dict`, weighting each pool cost basis by the amt of coins held in each pool. Then multiplies this
        average cost by the number of coins in the pool.
        """
        pools = queue_dict[coin]
        sum_weighted_costs = 0
        sum_amts = 0
        for p in pools:
            sum_weighted_costs += (p['cost_basis']/p['amt']) * p['amt']
            sum_amts += p['amt']
        sum_weighted_costs += (tradepool['cost_basis']/tradepool['amt']) * tradepool['amt']
        sum_amts += tradepool['amt']
        avg_cost = sum_weighted_costs / sum_amts
    #     if avg_cost > 3000:
    #         log.debug(coin)
    #         for p in pools:
    #             if p['cost_basis']/p['amt'] > 2000:
    #                 log.debug(p)
    #         raise Exception('wtf')
        log.debug('avg cost basis computed is %f', avg_cost)
        return avg_cost * tradepool['amt']

    def log_crypto_trade(self, fifo_queue_dict, depleted_pools, old_pool_amt, new_pool_amt, used_coin, used_amt):
        if not log.isEnabledFor(logging.DEBUG):
            return
        if len(depleted_pools) == 0:
                log.debug(f'Had {old_pool_amt} {used_coin}, reduced by {used_amt}, now {new_pool_amt} {used_coin}')
        else:
            depleted_amts = [str(d['amt']) for d in depleted_pools]
            dep_str = ", ".join(depleted_amts)
            current_pool_withdrawal = old_pool_amt - new_pool_amt
            log.debug(f'sold {dep_str} {used_coin} in {len(depleted_pools)} pool(s) and '
                    f'{current_pool_withdrawal} {used_coin} in current pool')
        coin_pools = [d["amt"] for d in fifo_queue_dict[used_coin]]
        total_pool_amt = sum(coin_pools)
        num_pools = len(coin_pools)
        log.debug(f'after sale, now holding {total_pool_amt} {used_coin} total in {num_pools} pool(s)')
        
    def log_fiat_trade(self, fifo_queue_dict, depleted_pools, old_pool_amt, new_pool_amt, coin, sold_amt):
        if not log.isEnabledFor(logging.DEBUG):
            return
        if len(depleted_pools) == 0:
            log.debug(f'Had {old_pool_amt} {coin}, reduced by {sold_amt} thru sale for USD')
        else:
            depleted_amts = [str(d['amt']) for d in depleted_pools]
            dep_str = ", ".join(depleted_amts)
            current_pool_withdrawal = old_pool_amt - new_pool_amt
            log.debug(f'sold {dep_str} {coin} in {len(depleted_pools)} pool(s) and '
                    f'{current_pool_withdrawal} {coin} in current pool')
        coin_pools = [d["amt"] for d in fifo_queue_dict[coin]]
        total_pool_amt = sum(coin_pools)
        num_pools = len(coin_pools)
        log.debug(f'after sale, now holding {total_pool_amt} {coin} total in {num_pools} pool(s)')

    def log_debug_info(self, fifo_queue_dict, row, used_coin, bought_coin, debug_date, debug_coin):
        if not log.isEnabledFor(logging.DEBUG):
            return
        if debug_date is None and debug_coin is None:
            return
        if debug_date in row['Date'] and (used_coin == debug_coin or bought_coin == debug_coin):
            if debug_coin in fifo_queue_dict:
                fq_display = [(d['acq_date'][:10], d['trade_date'][:19], round(d['amt'], 6), \
                            round(d['usd_price'], 6), round(d['cost_basis'], 6)) \
                            for d in fifo_queue_dict[debug_coin]]
                # fq_display = [dict(zip(['acdate', 'tdate', 'amt', 'price', 'cost'], t)) for t in fq_display]
                # fq_display = pd.DataFrame(fq_display)
                log.debug('=== DEBUG ===')
                log.debug("Coin: %s", debug_coin)
                log.debug('acq_date, trade_date, amt, price, cost')
                for r in fq_display:
                    log.debug(str(r))
                log.debug('=== END DEBUG ===')
            else:
                log.debug(f'{debug_coin} not in FIFO queue.')

    def get_received_usd_value_in_trade(self, bought_coin, row, received_amt_in_txn, total_amt_received_in_trade, 
                                        use_trade_prices_for_fmv=True):
        """
        return at-trade USD value of coin received in trade
        """
        # TODO: make sure bar returned is one that ENDS on hour-time of time. So if trade is 4:43 use the hour bar 
        # that ends at 5 pm!
        action = row['Action']
        if action != "BUY" and action != "SELL":
            raise Exception(f"Unknown action {row['Action']}")
        if use_trade_prices_for_fmv:
            if row['Currency'] == 'BTC':
                base_usd_price = self.p.get_hour_bar('BTC', 'USD', self.p.tradedate2ts(row['Date']), '')
            elif row['Currency'] == 'USD':
                base_usd_price = 1
            elif row['Currency'] in STABLE_COINS:
                base_usd_price = self.p.get_hour_bar(row['Currency'], 'USD', self.p.tradedate2ts(row['Date']))
                if base_usd_price < 1e-8:
                    raise Exception(f"cannot find base {row['Currency']} price", base_usd_price)
            elif row['Currency'] in SAFE_PRICE_CURRENCIES:
                try:
                    base_btc_price = self.p.get_hour_bar(row['Currency'], 'BTC', self.p.tradedate2ts(row['Date']), 
                                                CURR_EXCHANGE_DICT[(self.client_code, row['Currency'])])
                except:
                    base_btc_price = self.p.get_hour_bar(row['Currency'], 'BTC', self.p.tradedate2ts(row['Date']))
                btc_usd_price = self.p.get_hour_bar('BTC', 'USD', self.p.tradedate2ts(row['Date']), '')
                base_usd_price = btc_usd_price * base_btc_price
                if base_usd_price < 1e-8:
                    if row['Currency'] in ['B@', 'WCT', 'MRT', 'RBX']:
                        log.error(f"Tried to get price of {row['Currency']} at {row['Date']} but couldn't; price was {base_usd_price}")
                        if int(row['Date'][5:7]) >= 5 and int(row['Date'][:4]) >= 2017:
                            raise Exception('JRE problem! Call Greg')
                        base_usd_price = 0
                    else:
                        raise Exception(f"cannot find base {row['Currency']} price on {row['Date']}", base_btc_price, base_usd_price)
            else:
                raise Exception('Unknown currency', row['Currency'])
            if action == 'BUY':
                coin_price = row['Price']
                if 0 < coin_price:
                    price = coin_price * base_usd_price
                    volume_col = 'Volume'
                    return (row[volume_col] * (received_amt_in_txn/total_amt_received_in_trade) * price, coin_price, 
                            base_usd_price, False)
                else:
                    # The price is zero, so this is an ICO or something. Thus, set the action to SELL so we
                    # fall through to the next if statement, and use the USD value of the proceeds as the FMV
                    # This will make the FMV the same as the cost/proceeds, which is what we should do for a
                    # price of zero, which represents no market, i.e. no liqudity, e.g. b/c a coin just ICO'd
                    # THERE MUST BE NO ZERO PRICE TRADES WITH A NON-USD CURRENCY IN THE DATA FOR THIS TO WORK!
                    action = 'SELL'
            if action == 'SELL':
                coin_price = 1
                price = coin_price * base_usd_price
                volume_col = 'Cost/Proceeds'
                return (row[volume_col] * (received_amt_in_txn/total_amt_received_in_trade) * price, coin_price, 
                        base_usd_price, False)
        else:
            # BELOW IS ALL OLD CODE FOR *NOT* USING PRICES FROM TRADES SHEET FOR CALC    
            cc_api_missing_exch_err_str = "e param is not valid the market does not exist for this coin pair"
            zero_price_err_str = "zero price"
            coin_missing_err_str = "There is no data for the symbol"
            base_coin_missing_err_str = "There is no data for the toSymbol"
            no_data_returned_str = "'Data': [],"
            invalid_sym_str = 'fsym param is invalid. '
            invalid_params_str = 'There was an error with the parameters, contact us at info@cryptocompare.com'
            # special situations here
            # if row['Currency'] == 'B@' and row['Symbol'] == 'RBX':
            #     base_btc_price = self.p.get_hour_bar('RBX', 'WAVES', self.p.tradedate2ts(row['Date']))
            #     base_usd_price = self.p.get_hour_bar('WAVES', 'BTC', self.p.tradedate2ts(row['Date'])) * base_btc_price
            #     base_usd_price *= self.p.get_hour_bar('BTC', 'USD', self.p.tradedate2ts(row['Date']), 'Coinbase')
            #     base_volume = row['Volume'] * (received_amt_in_txn/total_amt_received_in_trade)
            #     base_coin = 'RBX'
            #     log.info('RBX base USD price on %s is %f', row['Date'], base_usd_price)
            #     return (base_volume * base_usd_price, 1, base_usd_price, True)
            # normal situations here
            try:
                if action == 'BUY':
                    coin_price = self.p.get_hour_bar(row['Symbol'], row['Currency'], self.p.tradedate2ts(row['Date']), row['Exchange'])
                    volume_col = 'Volume'
                else:
                    assert(action == "SELL")
                    coin_price = 1
                    volume_col = 'Cost/Proceeds'
                base_usd_price = self.p.get_hour_bar(row['Currency'], 'USD', self.p.tradedate2ts(row['Date']), '')
                if base_usd_price < 1e-8:
                    base_usd_price = self.p.get_hour_bar(row['Currency'], 'USD', self.p.tradedate2ts(row['Date']), '')
                    if base_usd_price < 1e-8:
                        raise Exception(f"Error: zero price for {row['Currency']}/USD on all exchanges!")
                if coin_price < 1e-8:
                    raise Exception(f"Error: zero price for {row['Symbol']}/{row['Currency']} on {row['Exchange']}!")
                price = coin_price * base_usd_price
                return (row[volume_col] * price, coin_price, base_usd_price, False)
            except Exception as e:
                # ----- error from CC API ----
                log.error("CC API error: %s", e.args)
                if any([cc_api_missing_exch_err_str in msg for msg in e.args]) \
                    or (zero_price_err_str in e.args[0]) \
                    or (coin_missing_err_str in e.args[0]) \
                    or (base_coin_missing_err_str in e.args[0]) \
                    or (no_data_returned_str in e.args[0]) \
                    or (invalid_sym_str in e.args[0]) \
                    or (invalid_params_str in e.args[0]):
                    if 'ico' in row['Exchange'].lower() \
                        or 'airdrop' in row['Exchange'].lower() \
                        or 'crowd sale' in row['Exchange'].lower():
                        log.info("ICO or Airdrop at %s", row['Exchange'])
                    else:
                        if row['Exchange'] not in SAFE_EXCHANGES:
                            log.error("Exchange is: %s and not present in safe exchanges %s", row['Exchange'], str(SAFE_EXCHANGES))
                            raise Exception(f"Unknown exchange {row['Exchange']} - trade is: \n {row}")
                        else:
                            log.info("Verified transaction at missing exchange: %s", row['Exchange'])
                    # exchange is missing from CC API, price is zero for coin, or no data on coin
                    # try to use currency-coin value involved in USD
                    # detect which col is base-coin volume
                    if total_amt_received_in_trade < 1e-10:
                        exmsg = f"Error: total amt received in trade is near zero!" \
                                + f" Total amt recvd: {total_amt_received_in_trade}," \
                                + f" In txn: {received_amt_in_txn}, " \
                                + f" Bought coin: {bought_coin}"
                        log.error(exmsg)
                        log.error("Row: \n%s", str(row))
                        raise Exception()
                    base_coin = row['Currency']
                    base_volume = row['Cost/Proceeds'] * (received_amt_in_txn/total_amt_received_in_trade)
                    try:
                        base_usd_price = self.p.get_hour_bar(base_coin, 'USD', self.p.tradedate2ts(row['Date']), '')
                        if base_usd_price < 1e-8:
                            base_usd_price = self.p.get_hour_bar(row['Currency'], 'USD', self.p.tradedate2ts(row['Date']), '')
                            if base_usd_price < 1e-8:
                                raise Exception(f"Error: zero price for {row['Currency']}/USD on all exchanges!")
                    except Exception as e:
                        # even the base currency's price can't be found? maybe curr and symbol are switched in data
                        # try symbol and volume
                        log.warning("Warning! Trying Symbol/Volume because base could not be found in USD")
                        log.warning("Old: Base coin %s Base volume %f", base_coin, base_volume)
                        base_coin = row['Symbol']
                        base_volume = row['Volume'] * (received_amt_in_txn/total_amt_received_in_trade)
                        log.warning("New: Base coin %s, Base volume %f", base_coin, base_volume)
                        try:
                            try:
                                base_usd_price = self.p.get_hour_bar(base_coin, 'USD', self.p.tradedate2ts(row['Date']), '')
                            except:
                                base_btc_price = self.p.get_hour_bar(base_coin, 'BTC', self.p.tradedate2ts(row['Date']))
                                if base_btc_price < 1e-8:
                                    base_coin = row['Currency']
                                    base_volume = row['Cost/Proceeds'] * (received_amt_in_txn/total_amt_received_in_trade)
                                    base_btc_price = self.p.get_hour_bar(base_coin, 'BTC', self.p.tradedate2ts(row['Date']))
                                    if base_btc_price < 1e-8 and base_coin not in WEIRD_BASE_COINS:
                                        raise Exception(f"Error: zero price for {base_coin}/BTC!")
                                base_usd_price = self.p.get_hour_bar('BTC', 'USD', self.p.tradedate2ts(row['Date']), '')
                                if base_usd_price < 1e-8:
                                    raise Exception(f"Error: zero price for BTC/USD!")
                                base_usd_price = base_usd_price * base_btc_price
                        except Exception as e2:
                            e2.args += (str(e))
                            log.error("Could not find base currency %s", e2)
                            raise e2
                    return (base_volume * base_usd_price, 1, base_usd_price, True)
                else:
                    # Some other error. Throw it
                    raise e
                # ----- error from CC API ----

    def add_to_8824(self, form8824, row, coin, pool_orig_acq_date, trade_cost_basis, amt_bought_in_txn, 
                    bought_coin, amt_used_in_txn, used_coin, total_amt_received_in_trade):
        """
        Adds 8824 txn to 8824 form.
        row: trade
        coin: str
        pool_orig_acq_date: str
        trade_cost_basis: float
        """
        given_up_amt_in_txn = amt_used_in_txn
        received_amt_in_txn = amt_bought_in_txn
        fmv, coin_p, base_usd_p, used_base = self.get_received_usd_value_in_trade(bought_coin, row, received_amt_in_txn,
                                                                            total_amt_received_in_trade)
        # adj_basis_volume_given_up = row['Cost/Proceeds'] + row['Fee']
        # given_up_coin_price_at_acquire = get_price_acquire(used_coin, row, pool)
        # adj_basis = given_up_coin_price_at_acquire * adj_basis_volume_given_up
        
        # if the trade is for basically nothing, don't add it
        if round(given_up_amt_in_txn, 8) == 0.0 and round(given_up_amt_in_txn, 8) == 0.0:
            return
        adj_basis = trade_cost_basis
        line_item_8824 = {
            "Like-Kind Property Given Up" : str(round(given_up_amt_in_txn, 8)) + " " + used_coin,
            "Coin Given Up": used_coin,
            "Like-Kind Property Received" : str(round(received_amt_in_txn, 8)) + " " + bought_coin,
            "Coin Received": bought_coin,
            "Date Originally Acquired" : pool_orig_acq_date,
            "Date of Actual Transfer" : row['Date'],
            "Date of Actual Receipt" : row['Date'],
            "Related Party" : "NO",
            "FMV of Like-Kind Property Received" : round(fmv, 8),
            "Adjusted Basis of Property Given Up" : round(adj_basis, 8),
            "Realized Gain" : round(fmv - adj_basis, 8),
            "deferred Gain" : round(self, fmv - adj_basis, 8),
            "Basis of Like-Kind Property Received" : round(adj_basis, 8),
            "Coin price in currency": coin_p,
            "Currency price": base_usd_p,
            "Error - used base": used_base
        }
        form8824 += [line_item_8824]
        log.debug('Added 8824 line item: %s', str(line_item_8824))
        
    def get_closing_fmv(self, volume, closing_price, cost):
        if closing_price >= 1e-8:
            return closing_price * volume
        return cost
        
    def get_closing_price(self, symbol):
        bases = QUOTE_COINS + STABLE_COINS
        eoy_ts = int(dt.datetime(self.tax_year+1,1,1,0,0,0).timestamp())
        try:
            usdprice = 0
            idx = 0
            while usdprice < 1e-8 and idx < len(bases):
                base = bases[idx]
                try:
                    if symbol == base:
                        baseprice = 1
                    else:
                        baseprice = self.p.get_hour_bar(symbol, base, hour_ts=eoy_ts, exchange='')
                except:
                    idx += 1
                    continue
                if base != "USD":
                    try:
                        usdprice = baseprice * self.p.get_hour_bar(base, "USD", hour_ts=eoy_ts, exchange='')
                    except:
                        idx += 1
                        continue
                else:
                    usdprice = baseprice * 1
                if usdprice >= 1e-8:
                    return usdprice
                idx += 1
            log.error("Cannot find price for {symbol} at end of year!")
            raise Exception("Cannot find price for {symbol} at end of year!")
        except Exception as e:
            return 0
        
    def compute_closing(self, fifo_queue_dict, form8824, form8949, calc_method):
        log.info('CLOSING:')
        formclosing = []
        avgcost = 0
        for k in fifo_queue_dict.keys():
            if len(fifo_queue_dict[k]) == 0:
                continue
            if calc_method == 'acb':
                avgcost = self.get_avg_cost(k, fifo_queue_dict)
            closing_amt = sum([d['amt'] for d in fifo_queue_dict[k]])
            if closing_amt >= 1e-8: # error_threshold(k, 1e-10, fifo_queue_dict[k][-1]['trade_date']):
                log.info(k, closing_amt)
                pools = fifo_queue_dict[k]
                # pools = pools[::-1]
                closing_price = self.get_closing_price(k)
                for pool in pools:
                    if calc_method == 'acb':
                        closing_entry = {
                            "Date": pool['trade_date'],
                            "Volume": round(pool['amt'], 20),
                            "Symbol": k,
                            "Price": round(pool["usd_price"], 20),
                            "Currency": "USD",
                            "Fee": 0,
                            "Cost": round(avgcost * pool['amt'], 20),
                            "FMV": self.get_closing_fmv(pool['amt'], closing_price, pool['cost_basis']),
                            "Source": pool['acq_date'],
                            "Source Key": pool['key']
                        }
                    else:    
                        closing_entry = {
                            "Date": pool['trade_date'],
                            "Volume": round(pool['amt'], 20),
                            "Symbol": k,
                            "Price": round(pool["usd_price"], 20),
                            "Currency": "USD",
                            "Fee": 0,
                            "Cost": round(pool['cost_basis'], 20),
                            "FMV": self.get_closing_fmv(pool['amt'], closing_price, pool['cost_basis']),
                            "Source": pool['acq_date'],
                            "Source Key": pool['key']
                        }
                    if closing_entry['Volume'] >= 1e-8:
                        formclosing += [closing_entry]
                    else:
                        log.warning(f"Ommitting {closing_entry}")
        return formclosing

    def save_all(self, form8824, form8949, formclosing):
        filepath8824 = os.path.join(self.base_dir, self.client_code, self.tax_year, f'{self.client_code.upper()}{self.tax_year} - Form 8824.csv')
        filepath8949 = os.path.join(self.base_dir, self.client_code, self.tax_year, f'{self.client_code.upper()}{self.tax_year} - Form 8949.csv')
        filepathclosing = os.path.join(self.base_dir, self.client_code, self.tax_year, f'{self.client_code.upper()}{self.tax_year} - Closing Positions.csv')
        cols8824 = [
            "Like-Kind Property Given Up",
            "Coin Given Up",
            "Like-Kind Property Received",
            "Coin Received",
            "Date Originally Acquired",
            "Date of Actual Transfer",
            "Date of Actual Receipt",
            "Related Party",
            "FMV of Like-Kind Property Received",
            "Adjusted Basis of Property Given Up",
            "Realized Gain",
            "Deferred Gain",
            "Basis of Like-Kind Property Received",
        ]
        cols8949 = [
            "Description (a)",
            "Date Acquired (b)",
            "Date Sold (c)",
            "Proceeds (d)",
            "Cost Basis (e)",
            "Adjustment Code (f)",
            "Adjustment amount (g)",
            "Gain or loss (h)",
        ]
        colsclosing = [
            "Date",
            "Volume",
            "Symbol",
            "Price",
            "Currency",
            "Fee",
            "Cost",
            "FMV",
            "Source",
        ]
        f8824_dates = ["Date Originally Acquired", "Date of Actual Transfer", "Date of Actual Receipt",]
        f8949_dates = ["Date Acquired (b)", "Date Sold (c)"]
        closing_dates = ["Date"]
        formatted8824 = pd.DataFrame(form8824)
        formatted8949 = pd.DataFrame(form8949) 
        formattedclosing = pd.DataFrame(formclosing)
        if len(formatted8824) > 0:
            for col in f8824_dates:
                formatted8824[col] = formatted8824[col].apply(lambda x: dtparser.parse(x).strftime("%m/%d/%Y"))
        if len(formatted8949) > 0:
            for col in f8949_dates:
                formatted8949[col] = formatted8949[col].apply(lambda x: dtparser.parse(x).strftime("%m/%d/%Y"))
        if len(formattedclosing) > 0:
            for col in closing_dates:
                formattedclosing[col] = formattedclosing[col].apply(lambda x: dtparser.parse(x).strftime("%m/%d/%Y"))
        # add empty columns in between existing ones
        #formatted8824 = formatted8824.reindex( columns = [cc for cc in [(c, i) for (c, i) in 
        #                                                zip(formatted8824.columns.tolist(), range(len(cols8824)))]])
        #formatted8949 = formatted8949.reindex( columns = [cc for cc in [(c, " ") for c in formatted8949.columns.tolist()]])
        #formattedclosing = formattedclosing.reindex( columns = [cc for cc in [(c, " ") for c in formattedclosing.columns.tolist()]])
        # write to files
        pd.DataFrame(formatted8824, columns=cols8824).to_csv(filepath8824, index=False, columns=cols8824)
        pd.DataFrame(formatted8949, columns=cols8949).to_csv(filepath8949, index=False, columns=cols8949)
        pd.DataFrame(formattedclosing, columns=colsclosing).to_csv(filepathclosing.replace("Positions", "Formatted Positions"), index=False, columns=colsclosing)
        pd.DataFrame(formclosing, columns=colsclosing).to_csv(filepathclosing, index=False, columns=colsclosing)

    def check_bought_amt_error(self, bought_amt_counter, bought_amt, bought_coin, row):
        trade_date = row['Date']
        buy_err = abs(bought_amt_counter - bought_amt)
        if buy_err > 1e-8:
            if buy_err > 2*self.p.get_error_threshold(bought_coin, buy_err, trade_date) and row['Date'] not in SAFE_TRADE_DATES:
                exmsg = f"ERROR: bought {bought_amt} {bought_coin} but counted up {bought_amt_counter} {bought_coin}!"
                log.error(exmsg)
                log.error("Triggering row: %s", str(row))
                raise Exception(exmsg)
            else:
                log.info(f"Bought {bought_amt} {bought_coin} but counted up {bought_amt_counter} {bought_coin}. USD value below "
                        " $0.01 so ignored; smaller value passed on.")
        return buy_err if bought_amt_counter - bought_amt > 1e-8 else 0
            
    def check_trades_validity(self, trades):
        # check to make sure Actions are correct
        actions = trades['Action'].unique().tolist()
        if not all([a in ['BUY', 'SELL'] for a in actions]):
            log.error(f"Not all actions are buy/sell! Actions in trades are {trades['Action'].unique()}")
            raise Exception('Major problem, not all actions are just buy/sell')
        exchanges = trades['Exchange'].unique().tolist()
        currs = trades['Currency'].unique().tolist()
        log.info(f"Exchanges present are: {exchanges}")
        log.info(f"Currencies present are: {currs}")
        # TODO: check to make sure exchanges are valid
        
    def initialize_fifo_queue(self, opening):
        fifo_queue_dict = {}
        if opening is None:
            log.info("Opening empty")
            return fifo_queue_dict
        # seed the fifo queue dict with all holdings from the opening positions of current year
        for i in range(len(opening)):
            opening_row = opening.iloc[i]
            if opening_row['Symbol'] in fifo_queue_dict:
                fifo_queue_dict[opening_row['Symbol']] += [{
                    "acq_date": opening_row['Source'],
                    "trade_date": opening_row['Date'],
                    "key": uuid.uuid4(),
                    "amt": opening_row['Volume'], 
                    "usd_price": opening_row['Cost'] / opening_row['Volume'],  # price per unit in original cost basis
                    "cost_basis": opening_row['Cost']
                }]
            else:
                fifo_queue_dict[opening_row['Symbol']] = [{
                "acq_date": opening_row['Source'],
                "trade_date": opening_row['Date'],
                "key": uuid.uuid4(),
                "amt": opening_row['Volume'], 
                "usd_price": opening_row['Cost'] / opening_row['Volume'],  # price per unit in original cost basis
                "cost_basis": opening_row['Cost']
            }]
        return fifo_queue_dict

    def add_income_and_spending_to_trades(self, trades, income, spending):
        from dateparser.conf import Settings as dtpsettings
        dtps = dtpsettings()
        dtps.RETURN_AS_TIMEZONE_AWARE = True
        # https://stackoverflow.com/questions/24284342/insert-a-row-to-pandas-dataframe/24287210
        # https://stackoverflow.com/questions/15888648/is-it-possible-to-insert-a-row-at-an-arbitrary-position-in-a-dataframe-using-pan?rq=1
        if (income is None) and (spending is None):
            return trades
        trades['d'] = pd.to_datetime(trades['Date'], utc=True)
        last_trade_date = trades.tail(1)["d"].iloc[0]
        first_trade_date = trades.head(1)["d"].iloc[0]
        # add income and spending to proper place in trades data rows
        if income is not None:
            for i in reversed(income.index):
                # add income trade
                row = income.iloc[i]
                dstr = row["Date"]
                d = dtp.parse(dstr, settings=dtps)
                insertdict = {
                    "Date": row['Date'],
                    "Action": 'BUY',
                    "Symbol": row['Symbol'],
                    "Exchange": "INCOME " + row["Action"],
                    "Volume": row["Volume"],
                    "Price": row["Price"],
                    "Currency": "USD",
                    "Fee": 0,
                    "FeeCurrency": "USD",
                    "Total": -1 * row['Total'],
                    "Cost/Proceeds": row['Total'],
                    "ExchangeId": "",
                    "Memo": "",
                    "d": d
                }    
                # income always goes first if there's a trade on the same datetime
                if d <= first_trade_date:
                    # insert at top of trades df
                    idx = 0
                    line = pd.DataFrame(insertdict, index=[idx])
                    trades = pd.concat([line, trades]).reset_index(drop=True)
                elif last_trade_date < d:
                    # insert at end of trades df
                    idx = len(trades) - 1
                    line = pd.DataFrame(insertdict, index=[idx])
                    trades = pd.concat([trades, line]).reset_index(drop=True)
                else:
                    # insert in middle
                    idx = trades[(trades['d'] >= d)].head(1).index[0]
                    line = pd.DataFrame(insertdict, index=[idx])
                    trades = pd.concat([trades.iloc[:idx], line, trades.iloc[idx:]]).reset_index(drop=True)
        if spending is not None:
            for i in reversed(spending.index):
                # add spend trade
                row = spending.iloc[i]
                dstr = row["Date"]
                d = dtp.parse(dstr, settings=dtps)
                insertdict = {
                    "Date": row['Date'],
                    "Action": 'SELL',
                    "Symbol": row['Symbol'],
                    "Exchange": "SPEND " + row["Action"],
                    "Volume": row["Volume"],
                    "Price": row["Price"],
                    "Currency": "USD",
                    "Fee": 0,
                    "FeeCurrency": "USD",
                    "Total": row["Total"],
                    "Cost/Proceeds": row["Total"],
                    "ExchangeId": "",
                    "Memo": "",
                    "d": d
                }
                # spending always goes last if there's a trade on the same datetime
                if d < first_trade_date:
                    # insert at top of trades df
                    idx = 0
                    line = pd.DataFrame(insertdict, index=[idx])
                    trades = pd.concat([line, trades]).reset_index(drop=True)
                elif last_trade_date <= d:
                    # insert at end of trades df
                    idx = len(trades) - 1
                    line = pd.DataFrame(insertdict, index=[idx])
                    trades = pd.concat([trades, line]).reset_index(drop=True)
                else:
                    # insert in middle
                    idx = trades[(trades['d'] > d)].head(1).index[0]
                    line = pd.DataFrame(insertdict, index=[idx])
                    trades = pd.concat([trades.iloc[:idx], line, trades.iloc[idx:]]).reset_index(drop=True)
        return trades

    def check_next_year_opening(self, closing):
        files = os.listdir(os.path.join(self.base_dir, self.client_code, self.tax_year+1))
        next_yr_idstr = self.client_code.upper() + str(self.tax_year+1)
        try:
            next_yr_opening_filename = [fn for fn in files 
                                        if (next_yr_idstr in fn)
                                        and (f'OPENING POSITIONS {self.tax_year+1}.csv' in fn)][0]
            next_yr_opening = pd.read_csv(os.path.join(self.base_dir, self.client_code, self.tax_year+1, next_yr_opening_filename))
            log.info(f'Loaded next year opening from {next_yr_opening_filename}')
        except (IndexError, FileNotFoundError):
            opening = None
            log.info(f'No next year opening statement found for {self.client_code}; closing/opening check skipped!')
            return
        closingdf = pd.DataFrame(closing)
        closingsums = closingdf.groupby('Symbol').sum()
        openingsums = next_yr_opening.groupby('Symbol').sum()
        log.info('Computed closing:')
        log.info(str(closingsums["Volume"]))
        log.info('Bitcoin.tax opening:')
        log.info(str(openingsums["Volume"]))
        oset = set(openingsums.index.tolist())
        cset = set(closingsums.index.tolist())
        if len(oset.difference(cset)) > 0:
            log.info("Totally missing some coins between computed closing and next year opening:")
            log.info(f"Computed closing coins: {closingsums.index.tolist()}")
            log.info(f"Nexr yr opening coins:  {openingsums.index.tolist()}")
            raise Exception("Missing coins between computed closing and next year opening!")
        differrs = 0
        symbs = oset.union(cset)
        for symb in symbs:
            if symb in openingsums.index and symb in closingsums.index:
                sumdiff = abs(closingsums.loc[symb]['Volume'] - openingsums.loc[symb]['Volume'])
            elif symb in openingsums.index:
                sumdiff = openingsums.loc[symb]['Volume']
            elif symb in closingsums.index:
                sumdiff = closingsums.loc[symb]['Volume']
            else:
                raise Exception("IMPOSSIBLE! Symbol taken from DFs not found in DFs")
            if sumdiff > 1e-6:
                thresh = self.p.get_error_threshold(symb, sumdiff, f"{self.tax_year}-06-30")
                if sumdiff > thresh:
                    log.info(f'Diff between closing and opening for {symb} is {sumdiff}; larger than thresh {thresh}')
                    differrs += 1
        if differrs > 0:
            log.info(f"Found {differrs} diffs between computed closing and next yr opening exceeding thresholds!!")
        log.info('Closing/opening check passed!')

    def gather_pool_stats_and_remove_empty_pools(self, fqd, stats, date):
        for k in fqd.keys():
            max_price = 0
            sum_prices = 0
            sum_prices_sq = 0
            wsum_prices = 0
            wsum_prices_sq = 0
            min_price = 1e20
            sum_amt = 0
            removed_cb_sum = 0
            n = len(fqd[k])
            for d in fqd[k]:
                cb = d['cost_basis']
                amt = d['amt']
                sum_amt += amt
                price = cb / amt
                if cb == np.nan or amt == np.nan or price == np.nan \
                or cb == np.inf or amt == np.inf or price == np.inf:
                    log.error('NaNs present! Amount: %f, Cost basis %f, Price: %f, Pool: %s', amt, cb, price, str(d))
                    if amt < 1e-8 and cb < 1e-4 and removed_cb_sum < 0.01:
                        removed_cb_sum += cb
                        fqd[k].remove(d)
                    elif removed_cb_sum >= 0.01:
                        exmsg = f'too many removed! sum of cost basis of removed pools is {removed_cb_sum}'
                        log.error(exmsg)
                        raise Exception(exmsg)
                    else:
                        exmsg = f'major error! too many pools with small amount and infinite price! Pool is: {fqd[k]}'
                        log.error(exmsg)
                        raise Exception(exmsg)
                if price > max_price: max_price = price
                if price < min_price: min_price = price
                sum_prices += price
                sum_prices_sq += price * price
                wsum_prices += price * amt
                wsum_prices_sq += price * price * amt
                if cb < 0:
                    exmsg = f'Invalid cost basis in {k} pools'
                    log.error(exmsg)
                    raise Exception(exmsg)
            if n > 0:
                stats += [{
                    'date': date,
                    'coin': k,
                    'maxprice': max_price,
                    'minprice': min_price,
                    'wavgprice': wsum_prices/sum_amt,
                    'wsdprice': wsum_prices_sq/sum_amt - (wsum_prices/sum_amt)**2,
                    'avgprice': sum_prices / n,
                    'sdprice': sum_prices_sq / n - (sum_prices/n)**2,
                    'pools': n,
                    'amt': sum_amt
                }]
            else:
                stats += [{
                    'date': date,
                    'coin': k,
                    'maxprice': 0,
                    'minprice': 0,
                    'wavgprice': 0,
                    'wsdprice': 0,
                    'avgprice': 0,
                    'sdprice': 0,
                    'pools': 0,
                    'amt': 0
                }]

    def run_fifo_calc(self, fifo_queue_dict):
        """
        Run calc
        """
        calc_method = self.calc_method
        fiat_calc_method = self.fiat_calc_method
        opening = self.opening
        trades = self.trades
        income = self.income
        spending = self.spending

        # lists of dicts going to 8949 CSV
        form8824 = []
        form8949 = []
        price_stats = []
        # number of times we merged a pool
        for i in range(len(trades)):
            row = trades.iloc[i]
            self.gather_pool_stats_and_remove_empty_pools(fifo_queue_dict, price_stats, row['Date'])
            log.info(row['Date'])
            curr = row['Currency']
            action = row['Action']
            coin = row['Symbol']
            trade_year = self.get_year_from_row_date(row['Date'])
            if curr == "USD":
                if action == 'SELL':
                    # sold crypto for USD; add to 8949 and reduce from correct crypto queue
                    sold_amt = row['Volume']
                    if log.isEnabledFor(logging.DEBUG):
                        log.debug(f'selling {sold_amt} {coin} through sale for USD')
                    # set up variables
                    unaccounted = sold_amt
                    pool_accumulator = 0; old_pool_amt = 0; new_pool_amt = 0; depleted_pools = []
                    # accumulate crypto from pools until volume is satisfied
                    while unaccounted > 0:
                        try:
                            pool = None
                            if fiat_calc_method == 'fifo' or fiat_calc_method == 'acb':
                                pool = fifo_queue_dict[coin][-1]
                            elif fiat_calc_method == 'lifo':
                                pool = fifo_queue_dict[coin][0]
                            if pool['amt'] < 1e-8 and coin == "BTC":
                                toadd = pool['amt']
                                if fiat_calc_method == 'fifo' or fiat_calc_method == 'acb':
                                    fifo_queue_dict[coin].pop()
                                    fifo_queue_dict[coin][-1]['amt'] += toadd
                                elif fiat_calc_method == 'lifo':
                                    fifo_queue_dict[coin].pop(0)
                                    fifo_queue_dict[coin][0]['amt'] += toadd
                                continue
                            if unaccounted < pool['amt']:
                                # transaction sold amt can be satisfied by current pool
                                old_pool_amt = pool['amt']
                                proceeds = row['Cost/Proceeds'] * (unaccounted/sold_amt)
                                if old_pool_amt < 1e-10 and (old_pool_amt * pool['usd_price']) >= 0.01 \
                                    and pool['usd_price'] < 2000:
                                    exmsg = "DIVISION BY NEARLY ZERO" \
                                        + f"\n Old pool amt: {old_pool_amt}" \
                                        + f"\n Unaccounted: {row}" \
                                        + f"\n Pool: {pool}" \
                                        + f"\n Row: {row}"
                                    log.error(exmsg)
                                    raise Exception(exmsg)
                                trade_cost_basis = None
                                if fiat_calc_method != 'acb':
                                    trade_cost_basis = pool['cost_basis'] * (unaccounted / old_pool_amt)
                                elif fiat_calc_method == 'acb':
                                    trade_cost_basis = self.get_avg_cost_basis(coin, pool, fifo_queue_dict) * (unaccounted / old_pool_amt)
                                # TODO: change pool['cost_basis'] here to the avg cost basis of the pools for the coin
                                # it should be the average price across pools of the coin times the number of coins in the pool?
                                # but are the prices going to be legi
                                new_pool_amt = old_pool_amt - unaccounted
                                if new_pool_amt < 0:
                                    exmsg = f'Negative pool amount: {new_pool_amt}' \
                                        + f"\n Pool: {pool}"
                                    log.error(exmsg)
                                    raise Exception(exmsg)
                                pool['amt'] = new_pool_amt
                                pool['cost_basis'] = pool['cost_basis'] * (new_pool_amt/old_pool_amt)
                                if fiat_calc_method == 'fifo' or fiat_calc_method == 'acb':
                                    fifo_queue_dict[coin][-1] = pool
                                elif fiat_calc_method == 'lifo':
                                    fifo_queue_dict[coin][0] = pool
                                # add to 8949
                                sold_amt_in_txn = unaccounted
                                # coin_price_on_trade_date = self.p.get_hour_bar(coin, 'USD', self.p.tradedate2ts(row['Date']), row['Exchange'])
                                # proceeds = coin_price_on_trade_date * row['Volume']
                                line_item_8949 = {
                                    "Description (a)" : str(round(sold_amt_in_txn,8)) + " " + coin,
                                    "Date Acquired (b)" : pool["acq_date"],
                                    "Date Sold (c)" : row['Date'],
                                    "Proceeds (d)" : round(proceeds, 8),
                                    "Cost Basis (e)" : round(trade_cost_basis, 8),
                                    "Adjustment Code (f)" : "",
                                    "Adjustment amount (g)" : "",
                                    "Gain or loss (h)" : round(proceeds - trade_cost_basis, 8),
                                }
                                if log.isEnabledFor(logging.DEBUG):
                                    log.debug('Added 8949 line item: %s', str(line_item_8949))
                                form8949 += [line_item_8949]
                                unaccounted = 0
                            else:
                                # transaction depletes current pool of crypto; subtract from order and go onto next in queue
                                depleted_pool = None
                                if fiat_calc_method == 'fifo' or fiat_calc_method == 'acb':
                                    depleted_pool = fifo_queue_dict[coin].pop()
                                elif fiat_calc_method == 'lifo':
                                    depleted_pool = fifo_queue_dict[coin].pop(0)
                                depleted_pools += [depleted_pool]
                                unaccounted -= depleted_pool['amt']
                                # add to 8949
                                sold_amt_in_txn = depleted_pool['amt']
                                # coin_price_on_trade_date = self.p.get_hour_bar(coin, 'USD', self.p.tradedate2ts(row['Date']), row['Exchange'])
                                proceeds = row['Cost/Proceeds'] * (sold_amt_in_txn/sold_amt)
                                trade_cost_basis = None
                                if fiat_calc_method != 'acb':
                                    trade_cost_basis = pool['cost_basis']
                                elif fiat_calc_method == 'acb':
                                    trade_cost_basis = self.get_avg_cost_basis(coin, pool, fifo_queue_dict)
                                line_item_8949 = {
                                    "Description (a)" : str(round(sold_amt_in_txn,8)) + " " + coin,
                                    "Date Acquired (b)" : depleted_pool["acq_date"],
                                    "Date Sold (c)" : row['Date'],
                                    "Proceeds (d)" : round(proceeds, 8),
                                    "Cost Basis (e)" : round(trade_cost_basis, 8),
                                    "Adjustment Code (f)" : "",
                                    "Adjustment amount (g)" : "",
                                    "Gain or loss (h)" : round(proceeds - trade_cost_basis, 8),
                                }
                                form8949 += [line_item_8949]
                        except KeyError as kex:
                            exmsg = ("FIFO queue key missing! Printing queue...")
                            for k in fifo_queue_dict.keys():
                                exmsg += "\n" + (f"{k}, {fifo_queue_dict[k]}")
                            exmsg += "\n" + ("KeyNotFound Exception %s", kex)
                            exmsg += "\n" + ("Row which caused FIFO queue key missing error: \n{row}")
                            log.error(exmsg)
                            raise Exception(exmsg)
                        except IndexError as iex:
                            log.error(f"Could not satisfy usage of {unaccounted} {coin} out of {sold_amt} sold")
                            thresh = self.p.get_error_threshold(coin, unaccounted, row['Date'])
                            log.error(f'Threshold is {thresh}')
                            if unaccounted > thresh:
                                exmsg = f"Amount of coin sold cannot be satisfied by amount in FIFO queue! Error: {iex}"
                                fq_display = [(d['acq_date'][:10], d['trade_date'][:19], round(d['amt'], 6), \
                                            round(d['cost_basis'], 6), round(d['usd_price'], 6)) \
                                            for d in fifo_queue_dict[used_coin]]
                                exmsg += "\n" + f"Triggering row: \n {row}"
                                exmsg += f"Used coin is {coin}"
                                exmsg += f"Printing queue:\n {fq_display}"
                                log.error(exmsg)
                                raise Exception(exmsg)
                            else:
                                unaccounted = 0
                    if log.isEnabledFor(logging.DEBUG):
                        self.log_fiat_trade(fifo_queue_dict, depleted_pools, old_pool_amt, new_pool_amt, coin, sold_amt)
                if action == 'BUY':
                    # bought crypto for USD, add to fifo_queue
                    if log.isEnabledFor(logging.DEBUG):
                        log.debug(f'adding {row["Volume"]} {coin} through buy for USD')
                    new_pool = {"acq_date" : row['Date'],
                                "trade_date": row['Date'],
                                "key": uuid.uuid4(),
                                "amt": row['Volume'], 
                                "usd_price": row['Cost/Proceeds']/row['Volume'],
                                "cost_basis": row['Cost/Proceeds']
                            }
                    if coin in fifo_queue_dict:
                        fifo_queue_dict[coin].insert(0, new_pool)
                    else:
                        fifo_queue_dict[coin] = [new_pool]
                    nowholding = sum([d['amt'] for d in fifo_queue_dict[coin]])
                    poollen = len(fifo_queue_dict[coin])
                    if log.isEnabledFor(logging.DEBUG):
                        log.debug(f'Now holding {nowholding} {coin} in {poollen} pools')
            else:
                # we exchanged crypto for crypto
                bought_coin = coin if action == "BUY" else curr
                used_coin = curr if action == "BUY" else coin
                if curr not in SAFE_CURRENCIES and curr not in SAFE_PRICE_CURRENCIES:
                    exmsg = f"Unknown currency {curr}"
                    log.error(exmsg)
                    raise Exception(exmsg)
                bought_amt = row['Volume'] if action == "BUY" else row['Cost/Proceeds']
                used_amt = row['Cost/Proceeds'] if action == "BUY" else row['Volume']
                # how many units of bought_coin do we get per unit of sold_coin?
                if log.isEnabledFor(logging.DEBUG):
                    self.log_debug_info(fifo_queue_dict, row, used_coin, bought_coin, self.debug_date, self.debug_coin)
                # sometimes on polo before 2018 they allowed buying/selling coins for less than 1e-8 BTC, so they recorded
                # the price as zero BTC. This fixes it.
                incorrect_trade_modified = False
                if (row['Price'] >= 0) and (row['Cost/Proceeds'] == 0) \
                    and (row['Currency'] in ['BTC', 'ETH']) \
                    and (trade_year < 2018) \
                    and (row['Volume'] < 1):
                    log.info("Found probable Poloniex trade with incorrect Cost/Proceeds")
                    row['Cost/Proceeds'] = 1e-8
                    row['Price'] = 1e-8
                    incorrect_trade_modified = True
                if (not incorrect_trade_modified) and (row['Cost/Proceeds'] == 0) and (action == "SELL"):
                    exmsg = "This seems to be a loss that should have Currency set to 'USD'. Change this row\n" + row
                    log.error(exmsg)
                    raise Exception(exmsg)
                if (not incorrect_trade_modified) and row['Price'] == 0 and (row['Cost/Proceeds'] != 0) and action == 'BUY':
                    exmsg = "No zero prices allowed! Please enter a real price for the zero at this row\n" + row
                    log.error(exmsg)
                    raise Exception(exmsg)
                incorrect_trade_modified = False
                # subtract sold from fifo queue
                if log.isEnabledFor(logging.DEBUG):
                    log.debug(f'selling %f %s', used_amt, used_coin)
                # set up variables
                unaccounted = used_amt
                pool_accumulator = 0; old_pool_amt = 0; new_pool_amt = 0; depleted_pools = []
                # accumulate crypto from pools until volume is satisfied
                bought_amt_counter = 0
                while unaccounted > 0:
                    try:
                        pool = None
                        if calc_method == 'fifo' or calc_method == "acb":
                            pool = fifo_queue_dict[used_coin][-1]
                        elif calc_method == 'lifo':
                            pool = fifo_queue_dict[used_coin][0]
                        else:
                            exmsg = f"invalid calc method '{calc_method}'"
                            log.error(exmsg)
                            raise Exception(exmsg)
                        if pool['amt'] < 1e-8 and used_coin == "BTC":
                            toadd = pool['amt']
                            if calc_method == 'fifo' or calc_method == "acb":
                                fifo_queue_dict[used_coin].pop()
                                fifo_queue_dict[used_coin][-1]['amt'] += toadd
                            elif calc_method == 'lifo':
                                fifo_queue_dict[used_coin].pop(0)
                                fifo_queue_dict[used_coin][0]['amt'] += toadd
                            else:
                                msg = f"invalid calc method '{calc_method}'"
                                log.error(msg)
                                raise Exception(msg)
                            continue
                        if unaccounted < pool['amt']:
                            # transaction sold amt can be satisfied by current pool
                            # first, use pool to "buy" bought coin, so that we pass originally acquired date along
                            amt_bought_in_txn = bought_amt * (unaccounted/used_amt)
                            old_pool_amt = pool['amt']
                            if (old_pool_amt < 1e-10) and (old_pool_amt * pool['usd_price']) >= 0.01:
                                exmsg = "DIVISION BY NEARLY ZERO" \
                                        + f"\n Old pool amt: {old_pool_amt}" \
                                        + f"\n Unaccounted: {row}" \
                                        + f"\n Pool: {pool}" \
                                        + f"\n Row: {row}"
                                log.error(exmsg)
                                raise Exception(exmsg)
                            trade_cost_basis = None
                            if calc_method == 'fifo' or calc_method == 'lifo':
                                trade_cost_basis = self.add_new_pool(fifo_queue_dict, row, pool['key'], pool['cost_basis'], 
                                                                pool['acq_date'], old_pool_amt, \
                                                                used_coin, unaccounted, bought_coin, amt_bought_in_txn, \
                                                                False)
                            elif calc_method == 'acb':
                                trade_cost_basis = self.add_new_pool_acb(pool, fifo_queue_dict, row, pool['key'], pool['cost_basis'], 
                                                                    pool['acq_date'], old_pool_amt, \
                                                                    used_coin, unaccounted, bought_coin, amt_bought_in_txn, \
                                                                    False)
                            if ((trade_cost_basis < 0.01 
                                and (unaccounted * pool['usd_price']) >= 0.025
                                and self.tax_year != 2018
                                and calc_method != "acb")
                                or (abs(trade_cost_basis) == float("inf"))):
                                exmsg = "ZERO OR INFINITE COST BASIS!" \
                                        + f"\n Calced CB was {trade_cost_basis} but pool value at purchase is {unaccounted * pool['usd_price']}" \
                                        + f"\n Pool: {pool}" \
                                        + f"\n Row: {row}"
                                log.error(exmsg)
                                raise Exception(exmsg)
                            new_pool_amt = pool['amt'] - unaccounted
                            if new_pool_amt < 0:
                                exmsg = f"Negative pool amount: {new_pool_amt} \n Pool: {pool}"
                                log.error(exmsg)
                                raise Exception(exmsg)
                            pool['amt'] = new_pool_amt
                            pool['cost_basis'] = pool['cost_basis'] * (new_pool_amt/old_pool_amt)
                            if calc_method == 'fifo' or calc_method == "acb":
                                fifo_queue_dict[used_coin][-1] = pool
                            elif calc_method == 'lifo':
                                fifo_queue_dict[used_coin][0] = pool
                            else:
                                msg = f"invalid calc method '{calc_method}'"
                                log.error(msg)
                                raise Exception(msg)
                            # add to 8824 or 8949
                            bought_amt_counter += amt_bought_in_txn
                            if trade_year < 2018:
                                self.add_to_8824(form8824, row, pool, pool['acq_date'], trade_cost_basis, \
                                            amt_bought_in_txn, bought_coin, unaccounted, used_coin, bought_amt)
                            else:
                                self.add_to_8949(form8949, row, pool, pool['acq_date'], trade_cost_basis, \
                                            amt_bought_in_txn, bought_coin, unaccounted, used_coin, bought_amt)
                            unaccounted = 0
                        else:
                            # transaction depletes current pool of crypto; subtract from order and go onto next in queue
                            # `pool` and `depleted_pool` are the same. this is confusing and pop should occur at end instead
                            depleted_pool = None
                            if calc_method == 'fifo' or calc_method == 'acb':
                                depleted_pool = fifo_queue_dict[used_coin].pop()
                            elif calc_method == 'lifo':
                                depleted_pool = fifo_queue_dict[used_coin].pop(0)
                            else:
                                raise Exception('invalid calc method')
                            if (depleted_pool['amt'] < 1e-10) \
                                and (depleted_pool['amt'] * depleted_pool['usd_price']) >= 0.01:
                                exmsg = "DIVISION BY NEARLY ZERO" \
                                        + f"\n Old pool amt: {old_pool_amt}" \
                                        + f"\n Unaccounted: {row}" \
                                        + f"\n Pool: {pool}" \
                                        + f"\n Row: {row}"
                                log.error(exmsg)
                                raise Exception(exmsg)
                            # first, use pool to "buy" bought coin, so that we pass originally acquired date along
                            amt_bought_in_txn = bought_amt * (depleted_pool['amt']/used_amt)
                            trade_cost_basis = None
                            if calc_method == 'fifo' or calc_method == "lifo":
                                trade_cost_basis = self.add_new_pool(fifo_queue_dict, row, pool['key'], pool['cost_basis'], \
                                                                pool['acq_date'], depleted_pool['amt'], used_coin, \
                                                                depleted_pool['amt'], bought_coin, amt_bought_in_txn, \
                                                                True)
                            elif calc_method == 'acb':
                                trade_cost_basis = self.add_new_pool_acb(pool, fifo_queue_dict, row, pool['key'], pool['cost_basis'], \
                                                                    pool['acq_date'], depleted_pool['amt'], used_coin, \
                                                                    depleted_pool['amt'], bought_coin, amt_bought_in_txn, \
                                                                    True)
                            else:
                                raise Exception('unknown calc method')
                            if ((trade_cost_basis < 0.01 
                                and (depleted_pool['amt'] * depleted_pool['usd_price']) >= 0.025 
                                and self.tax_year != 2018
                                and calc_method != "acb")
                                or (abs(trade_cost_basis) == float("inf"))):
                                exmsg = "Potential zero/infinite cost basis detected!"
                                exmsg += "\n"
                                exmsg += (f"Calced CB was {trade_cost_basis} but pool value at purchase is "
                                          f"{depleted_pool['amt'] * depleted_pool['usd_price']}")
                                exmsg += "\n" + f"Trade cost basis calculated is: {trade_cost_basis}"
                                exmsg += "\n" + "Last depleted pools in transaction:"
                                exmsg += "\n" + str(depleted_pool)
                                exmsg += "\n" + '-----------'
                                exmsg += "\n" + "All pools depleted in transaction:"
                                exmsg += "\n" + str(depleted_pools)
                                exmsg += "\n" + '-----------'
                                exmsg += "\n" + 'Trade that triggered this:'
                                exmsg += "\n" + str(row)
                                log.error(exmsg)
                                raise Exception(exmsg)
                            depleted_pools += [depleted_pool]
                            unaccounted -= depleted_pool['amt']
                            bought_amt_counter += amt_bought_in_txn
                            # add to 8824
                            if trade_year < 2018:
                                self.add_to_8824(form8824, row, depleted_pool, pool['acq_date'], trade_cost_basis, \
                                            amt_bought_in_txn, bought_coin, depleted_pool['amt'], used_coin, bought_amt)
                            else:
                                self.add_to_8949(form8949, row, depleted_pool, pool['acq_date'], trade_cost_basis, \
                                            amt_bought_in_txn, bought_coin, depleted_pool['amt'], used_coin, bought_amt)
                    except KeyError as kex:
                        exmsg = ("FIFO queue key missing! Printing queue...")
                        for k in fifo_queue_dict.keys():
                            exmsg += "\n" + (f"{k}, {fifo_queue_dict[k]}")
                        exmsg += "\n" + ("KeyNotFound Exception %s", kex)
                        exmsg += "\n" + ("Row which caused FIFO queue key missing error: \n{row}")
                        log.error(exmsg)
                        raise Exception(exmsg)
                    except IndexError as iex:
                        log.error(f"Could not satisfy usage of {unaccounted} {used_coin} out of {used_amt} sold")
                        thresh = self.p.get_error_threshold(used_coin, unaccounted, row['Date'])
                        log.error(f'Threshold is {thresh}')
                        if unaccounted > thresh:
                            exmsg = f"Amount of coin sold cannot be satisfied by amount in FIFO queue! Error: {iex}"
                            fq_display = [(d['acq_date'][:10], d['trade_date'][:19], round(d['amt'], 6), \
                                        round(d['cost_basis'], 6), round(d['usd_price'], 6)) \
                                        for d in fifo_queue_dict[used_coin]]
                            exmsg += "\n" + f"Triggering row: \n {row}"
                            exmsg += f"Used coin is {used_coin}"
                            exmsg += f"Printing queue:\n {fq_display}"
                            log.error(exmsg)
                            raise Exception(exmsg)
                        else:
                            unaccounted = 0
                buyerr = self.check_bought_amt_error(bought_amt_counter, bought_amt, bought_coin, row)
                if buyerr > 0:
                    fifo_queue_dict[bought_coin][0]['amt'] += buyerr
                if log.isEnabledFor(logging.DEBUG):
                    self.log_crypto_trade(fifo_queue_dict, depleted_pools, old_pool_amt, new_pool_amt, used_coin, used_amt)
                    self.log_debug_info(fifo_queue_dict, row, used_coin, bought_coin, self.debug_date, self.debug_coin)
            log.info('--------')
        formclosing = None
        #try:
        formclosing = self.compute_closing(fifo_queue_dict, form8824, form8949, calc_method)
        self.save_all(form8824, form8949, formclosing)
        self.check_next_year_opening(formclosing)
        #except Exception as e:
        #    log.error("Exception upon trying to save files:", e)
        log.info('-------------')
        return form8824, form8949, formclosing, price_stats

    def calculate(self, trades, income, spending, opening):
        self.check_trades_validity(trades)
        trades = self.add_income_and_spending_to_trades(trades, income, spending)
        trades.to_csv(os.path.join(self.base_dir, self.client_code, f'{self.client_code.upper()}{self.tax_year} - All Transactions.csv'), index=False)
        log.info('Saved all transactions file')
        fifo_queue_dict = self.initialize_fifo_queue(opening)
        log.info("Logging opening:")
        for k1 in fifo_queue_dict:
            log.info(f"====== {k1} {sum([d['amt'] for d in fifo_queue_dict[k1]])} TOTAL ======")
        #     for d1 in fifo_queue_dict[k1]:
        #         log.debug("Acq", 
        #               d1['acq_date'], 
        #               "Traded", 
        #               d1['trade_date'], 
        #               'Amt', 
        #               f"{round(d1['amt'], 8)}".ljust(8), 
        #               'Cost', 
        #               f"{round(d1['cost_basis'],8)}".ljust(8),
        #              'Key', str(d1['key'])[:5])
        self.opening = opening
        self.trades = trades
        self.income = income
        self.spending = spending
        form8824, form8949, closing, stats = self.run_fifo_calc(fifo_queue_dict)
        # TODO: run zero cost basis check?