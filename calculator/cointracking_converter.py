# coding: utf-8
# pylint: disable=missing-class-docstring, missing-function-docstring, line-too-long, superfluous-parens, trailing-whitespace, 
# pylint: disable=invalid-name, no-self-use, logging-fstring-interpolation, too-many-locals, missing-module-docstring, too-many-arguments
# pylint: disable=too-many-branches, missing-final-newline
import os
import datetime as dt
import logging
import pandas as pd

from utils import try_load_data_file
from config import CURRENCIES, STABLE_COINS, QUOTE_COINS

log = logging.getLogger()

purchase_coins = CURRENCIES + STABLE_COINS + QUOTE_COINS

def get_buy_sell_info(row):
    coin_found = False
    for c in purchase_coins:
        if (row["Cur..1"] == c) or (row["Cur."] == c):
            if (row["Cur..1"] == c):
                act = "BUY"
                curr = c
                sym = row['Cur.']
                vol = row['Buy']
                cost = row['Sell']
            else:
                act = "SELL"
                curr = c
                sym = row['Cur..1']
                vol = row['Sell']
                cost = row['Buy']    
            coin_found = True
            break
    if not coin_found:
        log.warning(f"CoinTracking conversion encountered unknown currency; trade is: \n {row}")
        act = "BUY"
        curr = row['Cur..1']
        sym = row['Cur.']
        vol = row['Buy']
        cost = row['Sell'] 
    return act, curr, sym, vol, cost  

def convert_ct_date(ctd):
    if type(ctd) == pd.Timestamp:
        return str(ctd) + " +0000"
    d = dt.datetime.strptime(ctd, '%d.%m.%Y %H:%M')
    return d.strftime('%Y-%m-%d %H:%M:00 +0000')

def convert_cointracking(client_code, tax_year, trades_filepath, 
                         income_filepath=None, spending_filepath=None, opening_filepath=None):
    log.info(f"Starting CoinTracking conversion for {client_code} {tax_year}")
    ct = try_load_data_file(trades_filepath, "CoinTracking trades", empty_df_if_none=False)
    income = try_load_data_file(income_filepath, "CoinTracking income", empty_df_if_none=True)
    spending = try_load_data_file(spending_filepath, "CoinTracking spending", empty_df_if_none=True)
    opening = try_load_data_file(opening_filepath, "CoinTracking opening", empty_df_if_none=True)
    trades = []

    # check for ICOs
    ico_keywords = ['ico', 'offering', 'sale', 'offer', 'distribute', 'presale', 'pre-']
    icokre = "|".join(ico_keywords)
    log.info("Logging CoinTracking ICOs for review:")
    icoidxs = ct.index[ct["Exchange"].str.lower().str.contains(icokre) | ct["Comment"].str.lower().str.contains(icokre) ].tolist()
    for icoidx in icoidxs:
        log.info(ct.iloc[icoidx])
        log.info("-------")
    log.info("Finished CoinTracking logging ICOs")

    log.info(f"CoinTracking exchanges are: {ct['Exchange'].unique()}")
    icodetected = False
    for idx in ct.index:
        row = ct.iloc[idx]
        # icodetected = 'ICO' in row['Exchange']
        # if icodetected:
        #     raise Exception('ico, please manually check what is going on in the file before continuing')
        if icodetected and (tax_year == 2016):
            icoactive = not icoactive
            if icoactive:
                bought = row['Cur.']
                vol = row['Buy']
            else:
                trades += [{
                    "Date": convert_ct_date(row["Trade Date"]),
                    "Action": "BUY",
                    "Symbol": bought, 
                    "Exchange": row["Exchange"],
                    "Volume": float(vol),
                    "Price" : 0,
                    "Currency" : row['Cur..1'],
                    "Fee" : 0,
                    "FeeCurrency" : row['Cur..1'],
                    "Total" : -1*float(row['Sell']),
                    "Cost/Proceeds": float(row['Sell']),
                    "ExchangeId": "",
                    "Memo": row['Comment'],
                }]
            continue
        if row["Type"] == "Trade":
            act, curr, sym, vol, cost = get_buy_sell_info(row)  
            # TODO: check correctness for ICOs 
            trades += [{
                "Date":  convert_ct_date(row["Trade Date"]),
                "Action": act,
                "Symbol": sym,
                "Exchange": row["Exchange"],
                "Volume": float(vol),
                "Price" : float(cost)/float(vol) if float(vol) > 0 else 0,
                "Currency" : curr,
                "Fee" : 0,
                "FeeCurrency" : curr,
                "Total" : (1 if act=="SELL" else -1)*float(cost),
                "Cost/Proceeds": float(cost),
                "ExchangeId": "",
                "Memo": row['Comment'],
            }]
        elif "income" in row["Type"].lower():
            trades += [{
                "Date":  convert_ct_date(row["Trade Date"]),
                "Action": "BUY",
                "Symbol": row['Cur.'],
                "Exchange": "INCOME " + " " + row["Comment"],
                "Volume": float(row['Buy']),
                "Price" : 0,
                "Currency" : "USD",
                "Fee" : 0,
                "FeeCurrency" : "USD",
                "Total" : 0 if len(str(row['Sell'])) <= 1 else -1 * float(row['Sell']),
                "Cost/Proceeds": 0 if len(str(row['Sell'])) <= 1 else float(row['Sell']),
                "ExchangeId": "",
                "Memo": row['Comment'],
            }]
        elif "spend" in row["Type"].lower():
            trades += [{
                "Date":  convert_ct_date(row["Trade Date"]),
                "Action": "SELL",
                "Symbol": row['Cur..1'],
                "Exchange": "SPENDING" + " " + row["Type"],
                "Volume": float(row['Sell']),
                "Price" : 0,
                "Currency" : "USD",
                "Fee" : 0,
                "FeeCurrency" : "USD",
                "Total" : 0 if (len(str(row['Buy'])) <= 1 or str(row['Buy']) == 'nan') else float(row['Buy']),
                "Cost/Proceeds": 0 if (len(str(row['Buy'])) <= 1  or str(row['Buy']) == 'nan') else float(row['Buy']),
                "ExchangeId": "",
                "Memo": row['Comment'],
            }]
        else:
            raise Exception(f"Unknown row type! \n {row}")
    for idx in income.index:
        row = income.iloc[idx]
        trades += [{
            "Date":  convert_ct_date(row["Trade Date"]),
            "Action": "BUY",
            "Symbol": row['Cur.'],
            "Exchange": "INCOME" + " " + row["Type"],
            "Volume": float(row['Buy']),
            "Price" : 0,
            "Currency" : "USD",
            "Fee" : 0,
            "FeeCurrency" : "USD",
            "Total" : 0 if (len(str(row['Sell'])) <= 1 or str(row['Sell']) == 'nan') else float(row['Sell']),
            "Cost/Proceeds": 0 if (len(str(row['Sell'])) <= 1 or str(row['Sell']) == 'nan') else float(row['Sell']),
            "ExchangeId": "",
            "Memo": row['Comment'],
            }]
    for idx in spending.index:
        row = spending.iloc[idx]
        trades += [{
            "Date":  convert_ct_date(row["Trade Date"]),
            "Action": "SELL",
            "Symbol": row['Cur..1'],
            "Exchange": "SPENDING" + " " + row["Type"],
            "Volume": float(row['Sell']),
            "Price" : 0,
            "Currency" : "USD",
            "Fee" : 0,
            "FeeCurrency" : "USD",
            "Total" : 0 if (len(str(row['Buy'])) <= 1 or str(row['Buy']) == 'nan') else float(row['Buy']),
            "Cost/Proceeds": 0 if (len(str(row['Buy'])) <= 1  or str(row['Buy']) == 'nan') else float(row['Buy']),
            "ExchangeId": "",
            "Memo": row['Comment'],
            }]
    colstrade = [
        "Date",
        "Action",
        "Symbol",
        "Exchange",
        "Volume",
        "Price",
        "Currency",
        "Fee",
        "FeeCurrency",
        "Total",
        "Cost/Proceeds",
        "ExchangeId",
        "Memo",
    ]
    tradesdf = pd.DataFrame(trades)
    tradesdf = tradesdf.iloc[::-1]
    tradesdf['d'] = pd.to_datetime(tradesdf['Date'])
    tradesdf = tradesdf.sort_values(by=['d'], kind='mergesort')
    file_dir = os.path.dirname(trades_filepath)
    filepathtrades = os.path.join(file_dir, f"{client_code.upper()}{tax_year} - TRADES.csv")
    tradesdf.to_csv(filepathtrades, index=False, columns=colstrade)
    log.info('CoinTracking conversion finished')
    log.info("If this is TRA's CoinTracking, check to make sure the Ark ICO in 2016 and the SDC/Particl ICO in 2017 came through correctly")