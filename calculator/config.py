# coding: utf-8
# pylint: disable=missing-class-docstring, missing-function-docstring, line-too-long, superfluous-parens, trailing-whitespace, 
# pylint: disable=invalid-name, no-self-use, logging-fstring-interpolation, too-many-locals, missing-module-docstring, too-many-arguments
# pylint: disable=too-many-branches, missing-final-newline
import logging
from dateutil import parser as dtparser

CURRENCIES = ['USD']
STABLE_COINS = ["USDT", "USDC", "TUSD", "DAI", "PAX", "HUSD"]
QUOTE_COINS = ["BTC", "ETH", "BNB", "HT", "BNT", "XRP", "LTC", "EOS", "CRO", "XTZ", "XLM", "TRX", "XMR", "WAVES", "NEO", "STORM"]
# forgot what this is or what was supposed to be in it
WEIRD_BASE_COINS = []
# exchanges that we know we'll have prices for
PRICEABLE_EXCHANGES = ['binance', 'bittrex', 'gemini', 'gdax', 'poloniex', 'liqui', 'coinbase', \
                       'cryptopia', 'huobipro', 'kucoin', 'hitbtc', "Bitstamp", 'GDAX', 'tidex', 'Tidex']
# exchanges missing from CC or w/missing pairs that are OK to use the BTC value involved to price transaction
SAFE_EXCHANGES = ['GuldenTrader', 'Poloniex', 'Bittrex', 'Bitfinex', 'Coinbase', 'Binance', 'Igor', 'Mintpal',\
                  'Cryptsy', 'Yobit', 'Cryptopia', 'LIQUI', 'Kucoin', 'Liqui', 'OTC', 'CCEX', 'coins e', \
                  'CRYPTSY', 'Polo-man', '1Broker', 'Bitmex', "Liquii", "Bitstamp", 'fx contract', 'CoinEX', \
                  'Kraken', 'Circle', 'Lykke', 'Poloneix', 'ADJ', 'MetalPay ICO', 'SALT ICO', 'TenX ICO',  \
                  'AIR ICO', 'BlockTix ICO', 'DNT ICO', 'TeleX ICO', 'Sirin Labs ICO', 'Polymath ICO', \
                  'BTC-e', 'RIG RENTAL', 'CoinExchange', 'MINING POOL', 'bitmex chg', 'General', 'stake', \
                  'Ark ICO', 'Particl ICO', 'Bitgrail', 'Waves', 'MSP ICO', 'PART - Staking', 'Bter', \
                  'Coinexchange', 'ICOS', 'EtherDelta', 'Shapeshift', 'Changelly', 'TradeSatoshi', 'GDAX',
                 'Tidex', 'Other', 'Waves Dex']
# zero cost basis trade exceptions; if trade is on this date
SAFE_TRADE_DATES = ['2018-10-11 05:45:06 +0000']
#SAFE_TRADE_DATES = ['2016-10-01 15:55:00 +0000', '2017-07-17 14:24:00 +0000',]
#                  '2017-05-11 19:00:35 +0000', '2017-06-10 02:16:50 +0000',
#                  '2017-08-08 02:16:50 +0000']
# safe currencies we've OKed
SAFE_CURRENCIES = ['BTC', 'USDT', 'ETH', 'XMR', 'DAO', 'LBC', 'NEO', 'LSK', 'LTC', 'PIVX', 'BCH', 'WAVES',
                  'ZEC', 'ZRX', 'SALT', 'B@', 'WCT', 'DCR', 'RBX', 'MRT', 'DOGE', 'SDC', 'JBS', 'USDC',
                  'VIDZ', 'BNB', 'ANS']

# currencies safe for using in price-from-csv mode
SAFE_PRICE_CURRENCIES = ['LSK', 'SDC', 'ETH', 'XMR', 'B@', 'WAVES', 'WCT', 'DCR', 'ETH', 'RBX', 'MRT', 'LTC',
                        'DOGE', "JBS", 'VIDZ', "TUSD", "USDC", "USDS", "PAX", "BNB", 'NEO', 'ANS', 'SENT', 'POLY']

# stable coins
STABLE_COINS = ['USDT', 'TUSD', 'USDS', 'PAX']

# maps currencies to exchanges so that the prices of these are gotten from said exchanges
# if the client "CLI" bought coin "CCCC" mostly at exchange "Xchange", then add ("CLI", "CCCC") : "Xchange"
CURR_EXCHANGE_DICT = {
    ("tra", 'SDC') : 'Poloniex',
    ("tra", 'LSK') : 'Bittrex',
    ("tra", 'USDT') : 'Bittrex',
    ("tra", 'ETH') : 'Bittrex',
    ("tra-later", 'SDC') : 'Poloniex',
    ("tra-later", 'LSK') : 'Bittrex',
    ("tra-later", 'USDT') : 'Bittrex',
    ("tra-later", 'ETH') : 'Bittrex',
    ("tra-earlier", 'SDC') : 'Poloniex',
    ("tra-earlier", 'LSK') : 'Bittrex',
    ("tra-earlier", 'USDT') : 'Bittrex',
    ("tra-earlier", 'ETH') : 'Bittrex',
    ("jre", 'XMR') : 'Bittrex',
    ("jre", 'WAVES') : 'Bittrex',
    ("jre", 'B@') : 'WavesDex',
    ("jre", 'WCT') : 'WavesDex',
    ("jre", 'RBX') : 'WavesDex',
    ("jre", 'MRT') : 'WavesDex',
    ("jre", 'DCR') : 'Bittrex',
    ("jre", 'ETH') : 'Bittrex',
    ("jre", 'LTC') : 'Bittrex',
    ("jre", 'DOGE') : 'Bittrex',
    ("nbo", 'JBS') : 'Bittrex',
    ("ded", 'ETH') : 'Coinbase',
    ("dch", 'ETH') : 'Coinbase',
    ("ani", 'ETH') : 'Coinbase Pro',
    ("mfi", 'ETH') : 'Coinbase',
    ("mva", 'VIDZ') : '',
    ("mva", 'ETH') : 'Coinbase',
    ('pfr', 'ETH') : 'Poloniex',
    ('tra-earlier', 'BNB') : 'Binance',
    ('tra-later', 'BNB') : 'Binance',
    ('tra-earlier', 'NEO') : 'Bittrex',
    ('tra-later', 'NEO') : 'Bittrex',
    ('tra-earlier', 'ANS') : 'Bittrex',
    ('tra-later', 'ANS') : 'Bittrex',
    ('nathans', 'ETH') : 'Bittrex',
    ('mbo', 'ETH') : 'Bittrex',
    ('jsm', 'ETH') : 'Bittrex',
    ('cla', 'ETH') : 'Bittrex',
    ('mra', 'ETH') : 'Kraken',
    ('nro', 'ETH') : 'Coinbase',
    ('mfi', 'NEO') : 'Bittrex',
    ('cha', 'ETH') : 'Binance',
    ('twa', 'ETH') : 'Binance',
    ('pfr', 'USDC') : "",
}