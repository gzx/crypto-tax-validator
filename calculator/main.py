# coding: utf-8
# pylint: disable=missing-class-docstring, missing-function-docstring, line-too-long, superfluous-parens, trailing-whitespace, 
# pylint: disable=invalid-name, no-self-use, logging-fstring-interpolation, too-many-locals, missing-module-docstring, too-many-arguments
# pylint: disable=too-many-branches, missing-final-newline
import os
import glob
import logging
import pickle as pkl
import pandas as pd

from cointracking_converter import convert_cointracking
from calculator import Calculator
from prices import PriceProvider
from utils import initialize_logging

initialize_logging()
log = logging.getLogger()


def do_cointracking_conversion(base_dir, client_code, tax_year):
    ct_filepaths = glob.glob(os.path.join(base_dir, client_code, str(tax_year), "*CoinTracking*"))
    trades_fp = next(filter(lambda f: "trade" in f.lower(), ct_filepaths), None)
    income_fp = next(filter(lambda f: "income" in f.lower(), ct_filepaths), None)
    spending_fp = next(filter(lambda f: "spend" in f.lower(), ct_filepaths), None)
    opening_fp = next(filter(lambda f: "open" in f.lower(), ct_filepaths), None)
    log.info(trades_fp)
    convert_cointracking(client_code, tax_year, trades_fp, income_fp, spending_fp, opening_fp)

def load_data_dfs(base_dir, client_code, tax_year, additional_trades_fn=None):
    """
    Returns (trades, income, spending, opening) pandas dataframes. 
    Additional trades filename may be passed in to integrate it with the trades file.
    """
    files = os.listdir(os.path.join(base_dir, client_code, tax_year))
    idstr = client_code.upper() + str(tax_year)
    idstrlastyr = client_code.upper() + str(tax_year-1)
    trades_filename = [fn for fn in files if 
                       idstr in fn 
                       and ((f'ALL TRADES {tax_year}.csv' in fn) 
                            or (f'ALL TRADES - {tax_year}.csv' in fn) 
                            or (f'TRADES.csv' in fn) 
                            or (f'{tax_year} - TRADES - {tax_year}.csv' in fn) 
                            or (f"{tax_year} - ALL TRADES.csv" in fn))
                       and 'lock' not in fn 
                       and "CoinTracking" not in fn][0]
    trades = pd.read_csv(os.path.join(base_dir, client_code,  trades_filename), parse_dates=True)
    if additional_trades_fn is not None:
        addl_trades = pd.read_csv(os.path.join(base_dir, client_code, additional_trades_fn), parse_dates=True)
        trades = pd.concat([trades, addl_trades], ignore_index=True)
        trades['d'] = pd.to_datetime(trades['Date'])
        trades = trades.sort_values(by=['d'], kind='mergesort')
        del addl_trades
    log.info(f'Loaded trades from {trades_filename}')
    try:
        opening_filename = [fn for fn in files if 
                            idstrlastyr in fn 
                            and f'Closing Positions.csv' in fn \
                            and 'lock' not in fn 
                            and "CoinTracking" not in fn][0]
        opening = pd.read_csv(os.path.join(base_dir, client_code, opening_filename))
        log.info(f'Loaded opening from {opening_filename}')
    except (IndexError, FileNotFoundError):
        try:
            opening_filename = [fn for fn in files if 
                                idstr in fn 
                                and ((f'OPENING POSITIONS {tax_year}.csv' in fn) 
                                     or (f'OPENING POSITIONS - {tax_year}.csv' in fn)
                                     or (f'OPENING - {tax_year}.csv' in fn)
                                     or (f"{tax_year} - OPENING.csv" in fn)
                                     or (f"{tax_year} - Opening Positions.csv" in fn))
                                and 'lock' not in fn 
                                and "CoinTracking" not in fn][0]
            opening = pd.read_csv(os.path.join(base_dir, client_code, opening_filename))
            if 'OPENING' or "Opening" in opening_filename:
                # reverse it if it's a bitcoin.tax opening, because we need oldest trades last
                opening = opening.reindex(index=opening.index[::-1])
            opening['Source'] = opening['Date']
            log.info(f'Loaded opening from {opening_filename}')
        except (IndexError, FileNotFoundError):
            opening = None
            log.info(f'No opening statement found for {client_code}')
    try:
        income_filename = [fn for fn in files if 
                           idstr in fn 
                           and ((f'INCOME {tax_year}.csv' in fn) 
                                or (f'INCOME - {tax_year}.csv' in fn) 
                                or (f'Income - {tax_year}.csv' in fn)
                                or (f'{tax_year} - Income.csv' in fn))
                           and 'lock' not in fn 
                           and "CoinTracking" not in fn][0]
        income = pd.read_csv(os.path.join(base_dir, client_code,  income_filename), parse_dates=True)
        log.info(f'Loaded income from {income_filename}')
    except (IndexError, FileNotFoundError):
        income = None
        log.info(f'No income statement found for {client_code}')
    try:
        spending_filename = [fn for fn in files if 
                             idstr in fn 
                             and ((f'SPENDING {tax_year}.csv' in fn) 
                                or (f'SPENDING - {tax_year}.csv' in fn) 
                                or (f'Spending - {tax_year}.csv' in fn) 
                                or (f'{tax_year} - Spending.csv' in fn))
                             and 'lock' not in fn 
                             and "CoinTracking" not in fn][0]
        spending = pd.read_csv(os.path.join(base_dir, client_code,  spending_filename), parse_dates=True)
        log.info(f'Loaded spending from {spending_filename}')
    except (IndexError, FileNotFoundError):
        spending = None
        log.info(f'No spending statement found for {client_code}')
    for df in [opening, trades, income, spending]:
        if df is not None and "Account" in df.columns:
            df.rename(columns={"Account":"Exchange"}, inplace=True)
    return (trades, income, spending, opening)

def run(base_dir, client_code, tax_year, coinvert, 
        calc_method, fiat_calc_method, 
        debug_date=None, debug_coin=None):
    if coinvert:
        do_cointracking_conversion(base_dir, client_code, tax_year)
    trades, income, spending, opening = load_data_dfs(base_dir, client_code, tax_year)
    price_provider = PriceProvider(base_dir)
    ## IMPORTANT: Before running, all prices must be nonzero and populated. All costs bases must be nonzero
    ## and populated for BUY trades. CHECK THIS IN EXCEL
    # TODO: put this check referenced above in the validator; call it here
    log.info(f"Starting calc - tax year: {tax_year}; client code: {client_code}")
    c = Calculator()
    c.calculate(base_dir, client_code, tax_year, 
                trades, income, spending, opening, 
                price_provider,
                calc_method, fiat_calc_method, 
                debug_date, debug_coin)


#### -------- RUN PARAMETERS AND EXECUTION ------------
if __name__ == "__main__":
    BASE_DIR = '/mnt/c/Users/Greg/Documents/tax-clients'
    CLIENT_CODE = 'jsm'
    TAX_YEAR = 2018
    COINVERT = True
    CALC_METHOD = "fifo"
    FIAT_CALC_METHOD = "fifo"
    # fill in if we want debug info on specific date for coin
    DEBUG_DATE = None #'2015-04-11 16:31:40 +0000' # '2016-07-11 16'
    DEBUG_COIN = None # 'RDD'    # 'XEM'
    # run it
    run(BASE_DIR, CLIENT_CODE, TAX_YEAR, COINVERT, CALC_METHOD, FIAT_CALC_METHOD, DEBUG_DATE, DEBUG_COIN)
#### -------- RUN PARAMETERS AND EXECUTION ------------