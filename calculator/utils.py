# coding: utf-8
# pylint: disable=missing-class-docstring, missing-function-docstring, line-too-long, superfluous-parens, trailing-whitespace, 
# pylint: disable=invalid-name, no-self-use, logging-fstring-interpolation, too-many-locals, missing-module-docstring, too-many-arguments
# pylint: disable=too-many-branches, missing-final-newline
import os
import datetime as dt
import logging
import multiprocessing_logging
import petname
import pandas as pd


def initialize_logging(logdir='.', debug=False):
    debugtag = "_debug" if debug else ""
    hrtag = petname.Generate(3)
    if not os.path.isdir(logdir):
        os.mkdir(logdir)
    logging.basicConfig(
        level=logging.INFO if not debug else logging.DEBUG,
        format="[%(asctime)s] %(levelname)s - %(message)s",
        datefmt='%Y-%m-%d %H:%M:%S',
        handlers=[
            logging.FileHandler(f"{logdir}/calculator{debugtag}_{hrtag}_{dt.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')}.log"),
            logging.StreamHandler()
        ]
    )
    multiprocessing_logging.install_mp_handler()
    logging.info(f"Logging initialized with ID '{hrtag}'")

def try_load_data_file(filepath, description, empty_df_if_none=False):
    """
    Try to load the file as a Pandas DataFrame; log process and result.
    - filepath (str): full path to file
    - description (str): description of file
    - empty_df_if_none (bool): return an empty pd.DataFrame() if filepath is None
    """
    log = logging.getLogger()
    if empty_df_if_none and filepath is None:
        log.info(f"file {filepath} is None, returning empty dataframe")
        return pd.DataFrame()
    try:
        log.info(f'loading {description} file {filepath}')
        ext = os.path.splitext(filepath)[1].lower()
        if ext == ".csv":
            return pd.read_csv(filepath)
        elif ext == ".xls" or ext == ".xlsx" or ext == ".xlsb":
            return pd.read_excel(filepath)
        else:
            raise Exception(f"Unknown file extension '{ext}'")
    except Exception as e:
        log.error(f"Could not load {description} file!")
        raise e