# coding: utf-8
# pylint: disable=missing-class-docstring, missing-function-docstring, line-too-long, superfluous-parens, trailing-whitespace, 
# pylint: disable=invalid-name, no-self-use, logging-fstring-interpolation, too-many-locals, missing-module-docstring, too-many-arguments
# pylint: disable=too-many-branches, missing-final-newline
import os
import time
import random
import requests
import logging
import pickle as pkl
import dateparser as dtp
import numpy as np
from dateutil import parser as dtparser


log = logging.getLogger()

class PriceProvider(object):

    def __init__(self, base_dir):
        self.base_dir = base_dir
        self.api_call_counter = 0
        self.api_call_hourly_limit = 8000
        self.api_key = "8abbcb76b57bd22488799d32d403affea24cc251d50826915cf551266c404c56"
        self.calc_start_time = time.time()
        self.cache_filepath = os.path.join(base_dir, 'cache.pkl')
        self.cache = self.load_cache(base_dir)

    def load_cache(self, base_dir):
        """ 
        establish cache of type [(symb, base, hour_ts, exch) -> bar dict]
        # cache[('EQB', 'BTC', 1487134800, '')] = {'price':0, 'high':0, 'low': 0, 'open':0, 'close':0}
        # cache[('EQB', 'BTC', 1511758800, '')] = {'price':0, 'high':0, 'low': 0, 'open':0, 'close':0}
        """
        try:
            with open(self.cache_filepath, 'rb') as f:
                return pkl.load(f)
        except FileNotFoundError as fnfe:
            log.warning('Price cache not found; initializing new price cache')
            return {}

    def get_cc_api_response(self, url):
        if self.api_call_counter >= self.api_call_hourly_limit - 5:
            log.warning('sleeping 15min due to CC API rate limit')
            left = requests.get('https://min-api.cryptocompare.com/stats/rate/hour/limit').json()['CallsLeft']['Histo']
            log.warning('CC API calls left was: %s', left)
            time.sleep(15*60+1)
        page = requests.get(url)
        self.api_call_counter += 1
        if time.time() > self.calc_start_time + 3600:  # 3600 seconds = 1 hour
            self.api_call_counter = 0
            self.calc_start_time = time.time()
        return page.json()

    def get_hour_bar(self, symb, base_symb, hour_ts, exchange='', bar_type='price'):        
        # if CLIENT_DIR == 'test':
        #     if symb == 'BTC' and base_symb == 'USD':
        #         if hour_ts < 1476489600: # oct 15th
        #             print('BTC is $10')
        #             return 10
        #         elif hour_ts < 1479168000: # nov 15th
        #             print('BTC is $40')
        #             return 40
        #         elif hour_ts < 1481760000: # dec 15th
        #             print('BTC is $50')
        #             return 50
        #     if symb == 'LBC' and base_symb == 'BTC':
        #         if exchange == 'Poloniex':
        #             print('LBC/BTC is 0.005')
        #             return 0.005
        #         if exchange == 'Bittrex':
        #             print('LBC/BTC is 0.01')
        #             return 0.01
        #         raise Exception('lbc')
        
        if (exchange.lower() == 'changelly'):
            exchange = ''
        if (exchange.lower() == 'coinbase pro'):
            exchange = 'Coinbase'
        
        #     if (exchange.lower() not in PRICEABLE_EXCHANGES)  \
        #         and ('ico' not in exchange.lower()) \
        #         and ('airdrop' not in exchange.lower()) \
        #         and ('crowd' not in exchange.lower()) \
        #         and ('dex' not in exchange.lower()) \
        #         and ('delta' not in exchange.lower()) \
        #         and ('relay' not in exchange.lower()) \
        #         and ('adj' not in exchange.lower()) \
        #         and ('igor' not in exchange.lower()) \
        #         and ('otc' not in exchange.lower()) \
        #         and ('oto' not in exchange.lower()) \
        #         and ('fork' not in exchange.lower()):
        #         exchange = ''
        
        if symb == 'ANS':
            symb = 'NEO'
        if base_symb == 'ANS':
            base_symb = 'NEO'
            
        #     if (hour_ts < 1539223200 and symb == "USDC"):
        #         # CC API does not have data for USDC before this point
        #         symb = "USDT"
        #     if (hour_ts < 1539223200 and base_symb == "USDC"):
        #         # CC API does not have data for USDC before this point
        #         base_symb = "USDT"
                
        #     if base_symb == 'B@' and symb == 'USD' and hour_ts < 1488326400:
        #         key = (symb, base_symb, hour_ts, exchange)
        #         self.cache[key] = {'price':0.5, 'high':0.5, 'low': 0.5, 'open':0.5, 'close':0.5}
        #     if base_symb == 'B@' and symb == 'BTC' and hour_ts < 1488326400:
        #         key = (symb, base_symb, hour_ts, exchange)
        #         self.cache[key] = {'price':0.00048642, 'high':0.00048642, 'low':0.00048642, 'open':0.00048642, 'close':0.00048642}
            
        #     if symb == 'USD':
        #         symb, base_symb = base_symb, symb
        #     if symb == base_symb:
        #         if symb == 'USD' or symb == 'BTC':
        #             return 1
        #         else:
        #             raise Exception(f'symbs are equal: {symb}')
        key = (symb, base_symb, hour_ts, exchange)    
        
        if key in self.cache and key[0] not in ['B@', 'WCT', 'RBX', 'MRT']:
            if random.random() < 1/25 and log.isEnabledFor(logging.DEBUG):
                log.debug('hit self.cache for key %s', key)
            if self.cache[key]['price'] < 1e-8:
                zeroprice = self.cache[key]['price']
                del self.cache[key]
                with open(self.cache_filepath, 'wb') as f:
                    pkl.dump(self.cache, f)
                log.warning(f"Found zero price of {zeroprice} for {key} in price cache; price removed; getting new price")
            else:
                return self.cache[key]['price']
        
        log.info("self.cache key %s requested", key)
        limit = 1
        url = 'https://min-api.cryptocompare.com/data/histohour?fsym={}&tsym={}&limit={}&toTs={}&api_key={}'\
                .format(symb.upper(), base_symb.upper(), limit, hour_ts, self.api_key)
        if exchange:
            if exchange == 'WavesDex':
                url += '&exchange={}'.format(exchange)
            else:
                url += '&e={}'.format(exchange)
        if random.random() < 1/25 and log.isEnabledFor(logging.DEBUG):
            log.debug("Hit CryptoCompare: %s", url)
        response = self.get_cc_api_response(url)
        data = response['Data']
        has_error = (response['Response'] != 'Success') or (len(response['Data']) == 0)
        if has_error:
            self.cache[key] = {'price':0, 'high':0, 'low': 0, 'open':0, 'close':0}
            exmsg = f"CC API ERROR:\n {response}"
            log.error(exmsg)
            raise Exception(exmsg)
        cache_updated = False
        for d in data:
            cache_updated = True
            d['price'] = (d['open'] + d['high'] + d['low'] + d['close']) / 4
        #         if d['price'] < 1e-8:
        #             print(f"Error, zero price: Price of {d['price']} on {symb}/{base_symb} at {exchange}!")
            dkey = (symb, base_symb, d['time'], exchange)
            self.cache[dkey] = d
        try:
            bar = [d for d in data if d['time'] == hour_ts][0]
        except Exception as e:
            exmsg = f"Error attempting to get price. CC API Details:\n {response}" \
                + f"Data: {data}" \
                + f"Data length: {len(data)}" \
                + f"URL: {url}"
            log.error(exmsg)
            raise Exception(exmsg) from e
        if cache_updated and random.random() < 1/1000:
            with open(self.cache_filepath, 'wb') as f:
                pkl.dump(self.cache, f)
        return bar[bar_type]

    def tradedate2ts(self, trade_date):
        if (len(trade_date) < 11) and ('/' in trade_date):
            # trade_date is of form "01/18/2015"; parse it
            trade_date = str(dtp.parse(trade_date))
        else:
            # trade_date is of form '2015-01-18 00:00:00 +0000', so remove ' +0000'
            trade_date = trade_date[:-6]
        # remove minutes and seconds to get hours
        dt_and_hr = np.datetime64(trade_date[:-5] + '00:00')
        return dt_and_hr.astype('uint64')

    def error_threshold(self, c, amt, trade_date):
        """
        Gets the amount of a coin `c` that was equal to $0.01 USD at time `trade_date`. Requires the amount of
        coin in the trade `amt`.
        :rtype: float describing the amount of coins equal to $0.01 USD at the time of the trade.
        """
        d = dtparser.parse(trade_date)
        tyear = d.year
        tmonth = d.month
        log.info(f"Getting error threshold for {c} at time {trade_date}")
        # heuristics: first, if amt in txn is small, just ignore it.
        # get the max price of ANY coin (except BTC and ZEC, which have been above $5k at times) in the year, 
        # and if the amt in the transaction is less than this, just return that threshold
        year_to_max_price = {
            2013: 100,
            2014: 300,
            2015: 20 if len(c) == 3 else 4,
            2016: 40,
            2017: 2000,
            2018: 3000,
            2019: 1000,
            2020: 2000
        }
        if (amt < 0.01/year_to_max_price[tyear]) and (c != "BTC") and (c != "ZEC"): 
            return 0.01/year_to_max_price[tyear]
        # if that doesn't work, actually calculate things
        try:
            # get the highest price in the hour to be safe, and divide $0.01 by it to get the amount 
            coin_btc_price = self.get_hour_bar(c, "BTC", self.tradedate2ts(trade_date), exchange='', bar_type='high')
            btc_usd_price = self.get_hour_bar("BTC", "USD", self.tradedate2ts(trade_date), exchange='', bar_type='price')
            return 0.01 / (coin_btc_price * btc_usd_price)
        except Exception as e:
            log.warning(f"Tried to get error threshold; got exception '{e}'")
            return self.get_error_threshold(c, amt, trade_date)

    def get_error_threshold(self, c, amt, trade_date):
        """
        Ignore errors produced when we can't fulfill a sale from the FIFO queue for a coin, but only if the amount 
        we need to fulfill is less than this constant. 
        This is for rounding errors, and is derived by taking the greatest price the crypto achieved over all time
        and multiplying it by the amount that would make it worth less than one penny.
        E.g. BTC was at most $1,300 in 2016, and so 0.01 / 1300 is the BTC equal to a penny or less.
        """
        d = dtparser.parse(trade_date)
        tyear = d.year
        tmonth = d.month
        log.info(f"Getting MANUALLY OVERRIDDEN error threshold for {c} at time {trade_date}")
        if tyear == 2016:
            if amt < 0.01/50 and c != 'ZEC':
                # no coin other than ZEC and BTC had in 2016 exceeded $50, so if it's below 0.01/50, it's ok to pass
                return 0.01/50
            if c == 'BTC': highest_price_achieved_in_year = 1300
            elif c == 'XEM': highest_price_achieved_in_year = 0.015
            elif c == 'DOGE': highest_price_achieved_in_year = 0.0005
            elif c == 'VRC': highest_price_achieved_in_year = 0.12
            elif c == 'USDT': highest_price_achieved_in_year = 1.05
            elif c == 'NAUT': highest_price_achieved_in_year = 0.78
            elif c == 'STRAT': highest_price_achieved_in_year = 0.12
            elif c == 'ETH': highest_price_achieved_in_year = 20
            elif c == 'ZEC': highest_price_achieved_in_year = 4300
            elif c == 'XMR': highest_price_achieved_in_year = 15
            elif c == 'DAO': highest_price_achieved_in_year = 0.0001
            elif c == "GEO": highest_price_achieved_in_year = 0.06
            elif c == "SEC": highest_price_achieved_in_year = 0.009
            elif c == "BLC": highest_price_achieved_in_year = 0.009
            elif c == "MYR": highest_price_achieved_in_year = 0.001
            # no coin other than BTC and ZEC had in 2016 exceeded $50, so if it's below 0.01/50, it's ok to pass
            else: highest_price_achieved_in_year = 50
            return 0.01 / highest_price_achieved_in_year
        elif tyear == 2017:
            if amt < 0.01/2000:
                # no coin other than BTC had in 2017 exceeded $2,000, so if it's below 0.01/2000, it's ok to pass
                return 0.01/2000
            if c == 'BTC': highest_price_achieved_in_year = 20000
            elif c == 'XEM': highest_price_achieved_in_year = 1.50
            elif c == 'MAID': highest_price_achieved_in_year = 1
            elif c == 'DOGE': highest_price_achieved_in_year = 0.02
            elif c == 'USDT': highest_price_achieved_in_year = 1.05
            elif c == 'STRAT': highest_price_achieved_in_year = 20
            elif c == 'ETH': highest_price_achieved_in_year = 1500
            elif c == 'ZEC': highest_price_achieved_in_year = 1500
            elif c == 'XMR' and tmonth < 3: highest_price_achieved_in_year = 15
            elif c == 'XMR' and tmonth < 8: highest_price_achieved_in_year = 50
            elif c == 'XMR': highest_price_achieved_in_year = 800
            elif c == 'RISE': highest_price_achieved_in_year = 0.0001
            elif c == 'XRP': highest_price_achieved_in_year = 5
            elif c == 'STR': highest_price_achieved_in_year = 1
            elif c == 'AMP': highest_price_achieved_in_year = 50
            elif c == 'PINK': highest_price_achieved_in_year = 0.06
            elif c == 'GNT': highest_price_achieved_in_year = 1.15
            elif c == 'SC' and tmonth < 3: highest_price_achieved_in_year = 0.00045
            elif c == 'SC' and tmonth < 6: highest_price_achieved_in_year = 0.01
            elif c == 'SC' and tmonth < 12: highest_price_achieved_in_year = 0.018
            elif c == 'SC' and tmonth == 12: highest_price_achieved_in_year = 0.03
            elif c == 'LTC' and tmonth < 8: highest_price_achieved_in_year = 5
            elif c == 'LTC' and tmonth < 12: highest_price_achieved_in_year = 100
            elif c == 'LTC' and tmonth == 12: highest_price_achieved_in_year = 350
            elif c == 'ARDR': highest_price_achieved_in_year = 2
            elif c == 'LBC': highest_price_achieved_in_year = 0.75
            elif c == 'NXC': highest_price_achieved_in_year = 0.55
            elif c == 'BAT': highest_price_achieved_in_year = 0.4
            elif c == 'BTG': highest_price_achieved_in_year = 480
            elif c == 'XLM': highest_price_achieved_in_year = 2
            elif c == 'MSP': highest_price_achieved_in_year = 0.41
            elif c == 'SYS': highest_price_achieved_in_year = 1
            elif c == 'EDG': highest_price_achieved_in_year = 3.20
            elif c == 'BCH' and tmonth < 11: highest_price_achieved_in_year = 650
            elif c == 'BCH' and tmonth >= 11: highest_price_achieved_in_year = 3500
            elif c == 'EQT': highest_price_achieved_in_year = 1.38
            elif c == 'MANA': highest_price_achieved_in_year = 0.26
            elif c == 'NXT': highest_price_achieved_in_year = 2.20
            elif c == 'BURST': highest_price_achieved_in_year = 0.11
            elif c == 'DGB': highest_price_achieved_in_year = 0.07
            elif c == 'BCN': highest_price_achieved_in_year = 0.02
            elif c == 'MYR': highest_price_achieved_in_year = 0.05
            elif c == 'BITB': highest_price_achieved_in_year = 0.001
            elif c == 'POT': highest_price_achieved_in_year = 0.4
            elif c == 'RBX': highest_price_achieved_in_year = 0.0001
            elif c == 'EMD': highest_price_achieved_in_year = 0.005
            elif c == 'HZ': highest_price_achieved_in_year = 0.1
            elif c == 'MNE': highest_price_achieved_in_year = 0.1
            elif c == 'Visio': highest_price_achieved_in_year = 0.001
            elif c == 'BTM': highest_price_achieved_in_year = 2
            elif c == "XRB": highest_price_achieved_in_year = 30
            elif c == "DCT": highest_price_achieved_in_year = 0.1
            else: highest_price_achieved_in_year = 2000
            return 0.01 / highest_price_achieved_in_year
        elif tyear == 2013:
            if amt < 0.01/1200:
                return 0.01/1200
            else: highest_price_achieved_in_year = 1200
            return 0.01 / highest_price_achieved_in_year
        elif tyear == 2014:
            if amt < 0.01/300:
                return 0.01/300
            elif c == "LTC": highest_price_achieved_in_year = 32
            elif c == "NJA": highest_price_achieved_in_year = 0.0008
            elif c == "VOOT": highest_price_achieved_in_year = 0.0005
            elif c == "YMC": highest_price_achieved_in_year = 0.0005
            elif c == "VRC": highest_price_achieved_in_year = 0.0005
            elif c == "IOC": highest_price_achieved_in_year = 0.0005
            elif c == "GB": highest_price_achieved_in_year = 0.0005
            elif c == "GNS": highest_price_achieved_in_year = 0.0005
            elif c == "SMBR": highest_price_achieved_in_year = 0.001
            else: highest_price_achieved_in_year = 300
            return 0.01 / highest_price_achieved_in_year
        elif tyear == 2015:
            if amt < 0.01/20:
                return 0.01/20
            elif c == "LTC": highest_price_achieved_in_year = 8
            elif c == "ETH": highest_price_achieved_in_year = 3.75
            elif c == "EVENT": highest_price_achieved_in_year = 0.00001
            elif c == "HZ": highest_price_achieved_in_year = 0.0001
            elif c == "RDD": highest_price_achieved_in_year = 0.001
            elif c == "XBS": highest_price_achieved_in_year = 0.001
            elif c == "URO": highest_price_achieved_in_year = 0.001
            elif c == "TRON": highest_price_achieved_in_year = 0.001
            elif c == "COVAL": highest_price_achieved_in_year = 0.001
            else: highest_price_achieved_in_year = 20
            return 0.01 / highest_price_achieved_in_year
        elif tyear == 2018:
            if amt < 0.01 / 20000:
                return 0.01/20000
            elif c == "ETH": highest_price_achieved_in_year = 1200
            elif c == "TLX": highest_price_achieved_in_year = 0.001
            elif c == "ANS": highest_price_achieved_in_year = 50
            elif c == "NEO": highest_price_achieved_in_year = 50
            elif c == "TZROP": highest_price_achieved_in_year = 1
            else: highest_price_achieved_in_year = 5000
            return 0.01 / highest_price_achieved_in_year
        elif tyear == 2019:
            if amt < 0.01 / 13000:
                return 0.01/13000
            elif c == "ETH": highest_price_achieved_in_year = 400
            elif c == "NIX": highest_price_achieved_in_year = 1
            else: highest_price_achieved_in_year = 13000
            return 0.01 / highest_price_achieved_in_year