from flask import Flask, request, jsonify, render_template, flash, redirect, url_for, send_from_directory, send_file, session
from flask_bootstrap import Bootstrap
from flask_appconfig import AppConfig
from flask_wtf import FlaskForm, RecaptchaField
from flask_wtf.file import FileField
from wtforms import TextField, HiddenField, ValidationError, RadioField,\
    BooleanField, SubmitField, IntegerField, FormField, validators
from werkzeug.utils import secure_filename
import werkzeug
from wtforms.validators import Required, InputRequired
import flask_excel
import os
import io
import time
import logging
import sys
import numpy as np
import pandas as pd
import datetime as dt
from dateutil import parser as dtp


LOGS_FOLDER = "logs-283jf9vg78"

class TradesForm(FlaskForm):
    # field1 = TextField('First Field', description='This is field one.')
    # field2 = TextField('Second Field', description='This is field two.', validators=[Required()])
    # hidden_field = HiddenField('You cannot see this', description='Nope')
    # recaptcha = RecaptchaField('A sample recaptcha field')
    # radio_field = RadioField('This is a radio field', choices=[
        # ('head_radio', 'Head radio'),
        # ('radio_76fm', "Radio '76 FM"),
        # ('lips_106', 'Lips 106'),
        # ('wctr', 'WCTR'),
    # ])
    # checkbox_field = BooleanField('This is a checkbox', description='Checkboxes can be tricky.')
    # # subforms
    # mobile_phone = FormField(TelephoneForm)
    # # you can change the label as well
    # office_phone = FormField(TelephoneForm, label='Your office phone')
    trades = FileField('Trades CSV upload', validators=[Required()])
    opening_type = RadioField('Type of Opening/Closing CSV', choices=[
        ('none', 'No opening or closing statement'),
        ('opening', 'Opening statement from Bitcoin.tax (first year of client calculation only)'),
        ('closing', "Closing statement created by calculator from prior tax year")
    ], validators=[InputRequired()])
    opening = FileField('Opening CSV or prior year Closing CSV upload (if there is one)')
    income = FileField('Income CSV upload (if there is one)')
    spending = FileField('Spending CSV upload (if there is one)')
    submit_button = SubmitField('Submit')

    def validate_trades(self, field):
        if self.trades.data.filename[-3:] != 'csv':
            raise ValidationError('Must upload a csv for trades. Refresh page to remove this error.')

    def validate_opening_type(self, field):
        if self.opening_type.data == None:
            raise ValidationError('Please select an opening type (None, Opening, or Closing). Refresh page to remove this error.')

    def validate_opening(self, field):
        try:
            if self.opening.data.filename[-3:] != 'csv':
                raise ValidationError('Must upload a csv for opening/closing. Refresh page to remove this error.')
        except AttributeError:
            pass
    
    def validate_income(self, field):
        try:
            if self.income.data.filename[-3:] != 'csv':
                raise ValidationError('Must upload a csv for income. Refresh page to remove this error.')
        except AttributeError:
            pass
    
    def validate_spending(self, field):
        try:
            if self.spending.data.filename[-3:] != 'csv':
                raise ValidationError('Must upload a csv for spending')
        except AttributeError:
            pass


def create_app(configfile=None):
    app = Flask(__name__)
    flask_excel.init_excel(app)
    # Flask-Appconfig is not necessary, but highly recommend =)
    # https://github.com/mbr/flask-appconfig
    AppConfig(app, configfile)
    Bootstrap(app)
    # in a real app, these should be configured through Flask-Appconfig
    app.config['SECRET_KEY'] = 'devkey'
    app.config['RECAPTCHA_PUBLIC_KEY'] = '6Lfol9cSAAAAADAkodaYl9wvQCwBMr3qGR_PPHcw'
    app.config['UPLOADS_FOLDER'] = "uploads-MXjBJCcMqIbwQs"
    app.config['REPORTS_FOLDER'] = "reports"
    if not os.path.isdir(app.config['UPLOADS_FOLDER']):
        os.mkdir(app.config['UPLOADS_FOLDER'])
    if not os.path.isdir(app.config['REPORTS_FOLDER']):
        os.mkdir(app.config['REPORTS_FOLDER'])

    def check_trades_validity(trades):
        # check to make sure Actions are correct
        actions = trades['Action'].unique().tolist()
        if not all([a in ['BUY', 'SELL'] for a in actions]):
            logging.info(f"Actions in trades are {trades['Action'].unique()}")
            return False
        return True
        # exchanges = trades['Exchange'].unique().tolist()
        # print(f"Exchanges present are: {exchanges}")
        
    def msg(s, is_invalid=True):
        now = dt.datetime.now
        return "[" + str(now()) + ("] ERROR: " if is_invalid else "] INFO: ") + s + "\r\n"

    def add_income_and_spending_to_trades(trades, income, spending):
        # https://stackoverflow.com/questions/24284342/insert-a-row-to-pandas-dataframe/24287210
        # https://stackoverflow.com/questions/15888648/is-it-possible-to-insert-a-row-at-an-arbitrary-position-in-a-dataframe-using-pan?rq=1
        if (income is None) and (spending is None):
            return trades
        trades['d'] = pd.to_datetime(trades['Date'])
        last_trade_date = trades.tail(1)["d"].iloc[0]
        first_trade_date = trades.head(1)["d"].iloc[0]
        # add income and spending to proper place in trades data rows
        if income is not None:
            for i in reversed(income.index):
                # add income trade
                row = income.iloc[i]
                dstr = row["Date"]
                d = dtp.parse(dstr) #.replace(tzinfo=None)
                insertdict = {
                    "Date": row['Date'],
                    "Action": 'BUY',
                    "Symbol": row['Symbol'],
                    "Exchange": "INCOME " + row["Action"],
                    "Volume": row["Volume"],
                    "Price": row["Price"],
                    "Currency": "USD",
                    "Fee": 0,
                    "FeeCurrency": "USD",
                    "Total": -1 * row['Total'],
                    "Cost/Proceeds": row['Total'],
                    "ExchangeId": "",
                    "Memo": "",
                    "d": d
                }    
                # income always goes first if there's a trade on the same datetime
                if d <= first_trade_date:
                    # insert at top of trades df
                    idx = 0
                    line = pd.DataFrame(insertdict, index=[idx])
                    trades = pd.concat([line, trades]).reset_index(drop=True)
                elif last_trade_date < d:
                    # insert at end of trades df
                    idx = len(trades) - 1
                    line = pd.DataFrame(insertdict, index=[idx])
                    trades = pd.concat([trades, line]).reset_index(drop=True)
                else:
                    # insert in middle
                    idx = trades[(trades['d'] >= d)].head(1).index[0]
                    line = pd.DataFrame(insertdict, index=[idx])
                    trades = pd.concat([trades.iloc[:idx], line, trades.iloc[idx:]]).reset_index(drop=True)
        if spending is not None:
            for i in reversed(spending.index):
                # add spend trade
                row = spending.iloc[i]
                dstr = row["Date"]
                d = dtp.parse(dstr) #.replace(tzinfo=None)
                insertdict = {
                    "Date": row['Date'],
                    "Action": 'SELL',
                    "Symbol": row['Symbol'],
                    "Exchange": "SPEND " + row["Action"],
                    "Volume": row["Volume"],
                    "Price": row["Price"],
                    "Currency": "USD",
                    "Fee": 0,
                    "FeeCurrency": "USD",
                    "Total": row["Total"],
                    "Cost/Proceeds": row["Total"],
                    "ExchangeId": "",
                    "Memo": "",
                    "d": d
                }
                # spending always goes last if there's a trade on the same datetime
                if d < first_trade_date:
                    # insert at top of trades df
                    idx = 0
                    line = pd.DataFrame(insertdict, index=[idx])
                    trades = pd.concat([line, trades]).reset_index(drop=True)
                elif last_trade_date <= d:
                    # insert at end of trades df
                    idx = len(trades) - 1
                    line = pd.DataFrame(insertdict, index=[idx])
                    trades = pd.concat([trades, line]).reset_index(drop=True)
                else:
                    # insert in middle
                    idx = trades[(trades['d'] > d)].head(1).index[0]
                    line = pd.DataFrame(insertdict, index=[idx])
                    trades = pd.concat([trades.iloc[:idx], line, trades.iloc[idx:]]).reset_index(drop=True)
        return trades

    def get_year_from_row_date(rowdate):
        return dtp.parse(rowdate).year
    
    def get_threshold(c, trade_year):
        if trade_year < 2017:
            if c == 'BTC':
                return 1e-5
            if len(c) != 3 and c != "DASH":
                return 1e-3
            return 5e-4
        if trade_year >= 2017:
            if c == 'BTC':
                return 5e-6
            if len(c) != 3 and c != "DASH":
                return 1e-3
            return 5e-5

    def remap_symbols(ttx):
        return ttx.replace("NEO", "ANS")
    
    def report_deficits(txns, opening):
        ret = []
        txns = remap_symbols(txns)
        holdings = None
        if opening is not None:
            sums = opening.groupby(by="Symbol").sum() 
            holdings = sums['Volume'].to_frame().to_dict()['Volume']
        else:
            holdings = {}
        coins = txns.Symbol.unique().tolist()
        currencies = txns.Currency.unique().tolist()
        tradeables = (coins + currencies)
        if any([t == "\t" or t == "" or t == " " for t in coins]):
            ret += [msg("Transactions exist with invalid coin names: \n " + str(txns[txns.Symbol == "\t"]))]
        if any([t == "\t" or t == "" or t == " " for t in currencies]):
            ret += [msg("Transactions exist with invalid currency names: \n " + str(txns[txns.Currency == "\t"]))]
        # ret += [msg("Holdings at start will appear below if any exist:")]
        for c in tradeables:
            if c not in holdings.keys():
                holdings[c] = 0
            if c == "USD":
                holdings[c] = 1e20
            # if holdings[c] > 0 and c != "USD":
            #     ret += [f"\t {c}: {round(holdings[c], 8)} \n"]
        ret += [
            msg('Validation started', False),
            msg("Coins traded: " + str(coins), False),
            msg("Currencies used: " + str(currencies), False),
        ]
        max_deficits = dict.fromkeys(tradeables)
        max_deficits_info = dict.fromkeys(tradeables)
        for c in tradeables:
            max_deficits[c] = 0
        for i in range(len(txns)):
            row = txns.loc[i]
            row = row.copy()  # turn off those pesky "Setting a value on a copy of a dataframe" warnings
            action = row['Action']
            try:
                trade_year = get_year_from_row_date(row['Date'])
            except:
                logging.info(str(row))
                logging.info('at row ' + str(i))
                raise
            curr = row['Currency']
            coin = row['Symbol']
            trade_date = row['Date']
            # first check if the trade row is malformed
            if not(np.isreal(row['Price']) and np.isreal(row['Volume']) and np.isreal(row['Cost/Proceeds'])):
                ret += [msg("Invalid price, volume, or cost/proceeds for this row: \n" + str(row))]
            if curr != "USD":
                incorrect_trade_modified = False
                is_loss_or_airdrop = False
                # sometimes on polo before 2018 they allowed buying/selling coins for less than 1e-8 BTC, so they recorded
                # the price as zero BTC. This fixes it.
                if (row['Price'] >= 0) and (row['Cost/Proceeds'] == 0) \
                    and (row['Currency'] in ['BTC', 'ETH']) \
                    and (trade_year < 2018) \
                    and (row['Volume'] < 1):
                    # ret += [msg("Found probable Poloniex trade with zero Cost/Proceeds; will be fixed by calculator.", False)]
                    row.loc['Cost/Proceeds'] = 1e-8
                    row.loc['Price'] = 1e-8
                    incorrect_trade_modified = True
                if (not incorrect_trade_modified) and (row['Cost/Proceeds'] == 0) and (action == "SELL"):
                    ret += [msg(f"This seems to be a loss that should have Currency set to 'USD'. Change this row: \n" + str(row))]
                    is_loss_or_airdrop = True
                if (not incorrect_trade_modified) and (row['Cost/Proceeds'] == 0) and (action == "BUY"):
                    ret += [msg(f"This seems to be an airdrop that should have Currency set to 'USD'. Change this row: \n" + str(row))]
                    is_loss_or_airdrop = True
                if (not is_loss_or_airdrop) and (not incorrect_trade_modified) and row['Price'] == 0 and (row['Cost/Proceeds'] != 0) and action == 'BUY':
                    ret += [msg(f"No zero prices allowed. Please enter a real price for the zero at this row: \n" + str(row))]
            if row['Volume'] == 0:
                ret += [msg(f"No rows with zero volume allowed. Please enter a real price for the zero at this row: \n" + str(row))]
            # edit the holdings
            if action == "BUY":
                holdings[coin] += row['Volume']
                holdings[curr] -= row['Cost/Proceeds']
            elif action == "SELL":
                holdings[coin] -= row['Volume']
                holdings[curr] += row['Cost/Proceeds']
            else:
                ret += [msg(f"INVALID ACTION! Major error. At row {i}: \n" + str(row))]
                continue
            # now check for deficits
            for c in [coin, curr]:
                # if c == "ANS":
                    # ret += [msg(f"At {row['Date']} ANS holdings are {holdings[c]}")]
                if (holdings[c] <= (-1 * get_threshold(c, trade_year))) and (max_deficits[c] == 0):
                    ret += [
                        msg(f"FIRST deficit of {round(holdings[c],4)} {c} caused at time {trade_date} in {action} of " + \
                            f"{round(row['Volume'], 8)} {row['Symbol']} for {round(row['Cost/Proceeds'], 8)} {row['Currency']}")
                    ]
                    max_deficits_info[c] = f"time {trade_date} in {action} of " + \
                                           f"{round(row['Volume'], 8)} {row['Symbol']} for {round(row['Cost/Proceeds'], 8)} {row['Currency']}"
                if holdings[c] <= max_deficits[c]:
                    max_deficits[c] = holdings[c]
                    max_deficits_info[c] = f"time {trade_date} in {action} of " + \
                                           f"{round(row['Volume'], 8)} {row['Symbol']} for {round(row['Cost/Proceeds'], 8)} {row['Currency']}"
        # print out max deficits
        ret += [msg(f"SUMMARY: LARGEST deficits and times of occurrence will appear below (if any exist)", False)]
        for c in max_deficits.keys():
            if max_deficits[c] <= (-1 * get_threshold(c, trade_year)):
                ret += [msg(f"LARGEST deficit of {round(max_deficits[c],4)} {c} was caused at {max_deficits_info[c]}")]
        return ret

    def preprocess_df(df):
        df['Date'] = df['Date'].str.replace('+0000', '', regex=False)
        df = df.dropna(subset=['Date', 'Symbol'])
        return df

    def validate(tradesfn, openingfn="", opening_is_opening=False, incomefn="", spendingfn=""):
        rpath = tradesfn[:-4].replace(' - ', '_').replace(' ', '_').replace(app.config['UPLOADS_FOLDER'], app.config['REPORTS_FOLDER']) \
                + "_" + dt.datetime.now().strftime("%y%m%d_%H%M%S") + "_REPORT.txt"
        msgs = []
        trades = preprocess_df(pd.read_csv(tradesfn))
        try:
            trades = trades[['Date', 'Action', 'Symbol', 'Exchange', 'Volume', 'Price', 'Currency', 'Fee', 'FeeCurrency', 'Total', 'Cost/Proceeds', 'ExchangeId', 'Memo',]]
        except IndexError:
            trades = trades[['Date', 'Action', 'Symbol', 'Account', 'Volume', 'Price', 'Currency', 'Fee', 'FeeCurrency', 'Total', 'Cost/Proceeds', 'ExchangeId', 'Memo',]]
            trades = trades.rename(columns={"Account": "Exchange"})
        except KeyError:
            trades = trades[['Date', 'Action', 'Symbol', 'Account', 'Volume', 'Price', 'Currency', 'Fee', 'FeeCurrency', 'Total', 'Cost/Proceeds', 'ExchangeId', 'Memo',]]
            trades = trades.rename(columns={"Account": "Exchange"})
        opening = None
        spending = None
        income = None
        if len(openingfn) > 0:
            opening = preprocess_df(pd.read_csv(openingfn))
            if opening_is_opening:  # reverse it if it's a bitcoin.tax opening, because we need oldest trades last
                opening = opening.reindex(index=opening.index[::-1])
                msgs += [msg("Opening is Bitcoin.tax style", False)]
            else:
                msgs += [msg("Opening is last year's closing", False)]
            opening['Source'] = opening['Date']
        if len(incomefn) > 0:
            income = preprocess_df(pd.read_csv(incomefn))
        if len(spendingfn) > 0:
            spending = preprocess_df(pd.read_csv(spendingfn))
        if not check_trades_validity(trades):
            msgs += [msg("Not all actions in trades are 'BUY' or 'SELL'")]
        txns = add_income_and_spending_to_trades(trades, income, spending)
        deficits = report_deficits(txns, opening)
        msgs += deficits
        with open(rpath, 'w') as f:
            f.writelines(msgs)
        logging.info("Report saved to " + rpath)
        return rpath

    @app.route('/', methods=['GET', 'POST'])
    def index():
        form = TradesForm()
        if form.validate_on_submit():
            session.pop('_flashes', None)
            tfpath = app.config['UPLOADS_FOLDER'] + "/" + secure_filename(form.trades.data.filename)
            logging.info("Filename uploaded is " + str(secure_filename(form.trades.data.filename)))
            form.trades.data.save(tfpath)
            ofpath = ""
            opening_is_opening = False
            ifpath = ""
            sfpath = ""
            logging.info("Opening type is " + str(type(form.opening_type.data)) + " " + str(form.opening_type.data))
            try:
                ofpath = app.config['UPLOADS_FOLDER'] + "/" + secure_filename(form.opening.data.filename)
                logging.info("Filename uploaded is " + str(secure_filename(form.opening.data.filename)))
                form.opening.data.save(ofpath)
                if (form.opening_type.data == "none"):
                    flash(f'You cannot select an opening file and mark it as "No opening or closing" on the selector. Please correct this error and close this message.',
                         'error')
                    return redirect(url_for('index'))
                opening_is_opening = (form.opening_type.data != "closing")
                logging.info('Opening is opening?: ' + str(opening_is_opening))
            except AttributeError:
                if (form.opening_type.data != "none"):
                    flash(f'You cannot select the "No opening or closing" option on the selector and upload an Opening file. Please correct this error and close this message.',
                         'error')
                    return redirect(url_for('index'))
            try:
                ifpath = app.config['UPLOADS_FOLDER'] + "/" + secure_filename(form.income.data.filename)
                logging.info("Filename uploaded is " + str(secure_filename(form.income.data.filename)))
                form.income.data.save(ifpath)
            except AttributeError:
                pass
            try:
                sfpath = app.config['UPLOADS_FOLDER'] + "/" + secure_filename(form.spending.data.filename)
                logging.info("Filename uploaded is " + str(secure_filename(form.spending.data.filename)))
                form.spending.data.save(sfpath)
            except AttributeError:
                pass
            try:
                report_path = validate(tfpath, ofpath, opening_is_opening, ifpath, sfpath)
                # flash(f'Your report is ready and should be downloading now', 'info')
                return send_file(report_path, as_attachment=True)
            except Exception as ex:
                logging.error("Exception in validator", exc_info=True)
                flash(f'An error occurred and your report could not be generated', 'error')
                return redirect(url_for('index'))
        return render_template('index.html', form=form)

    return app

if __name__ == '__main__':
    if not os.path.isdir(LOGS_FOLDER):
        os.mkdir(LOGS_FOLDER)
    root = logging.getLogger()
    logging.basicConfig(filename=LOGS_FOLDER + '/' + 'app.log', filemode='w', format='%(asctime)s - %(levelname)s - %(message)s')
    root.setLevel(logging.DEBUG)
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    root.addHandler(handler)
    create_app().run(host="0.0.0.0", port=14200, debug=False)



# flash('critical message', 'critical')
# flash('error message', 'error')
# flash('warning message', 'warning')
# flash('info message', 'info')
# flash('debug message', 'debug')
# flash('different message', 'different')
# flash('uncategorized message')

# @app.route("/upload", methods=['GET', 'POST'])
# def upload_file():
#     if request.method == 'POST':
#         return jsonify({"trades": request.get_array(field_name='trades'), "opening": request.get_array(field_name='opening')})
#     return 
# #     return '''
# #     <!doctype html>
# #     <title>Upload an excel file</title>
# #     <h1>Trades file upload (csv, tsv, csvz, tsvz only)</h1>
# #     <form action="" method=post enctype=multipart/form-data>
# #     <p>
# #     <input type=file name=trades>
# #     <input type=file name=opening>
# #     <input type=submit value=Upload>
# #    </form>
# #     '''



# @app.route("/export", methods=['GET'])
# def export_records():
#     return flask_excel.make_response_from_array([[1,2], [3, 4]], "csv", file_name="export_data")

# if __name__ == "__main__":
#     app.run()
